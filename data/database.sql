-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Dim 07 Juillet 2019 à 21:15
-- Version du serveur :  10.1.13-MariaDB
-- Version de PHP :  5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `esto_test_2019`
--

-- --------------------------------------------------------

--
-- Structure de la table `contact_informations`
--

CREATE TABLE `contact_informations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `contact_informations`
--

INSERT INTO `contact_informations` (`id`, `user_id`, `address`, `zip_code`, `region`, `city`, `phone`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'RUE 499 N 06 HAY ZELLAKA', '60300', '9', 'Berkane', '0677095575', '2019-07-07 16:30:13', '2019-07-07 16:30:13', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `curriculum_vitaes`
--

CREATE TABLE `curriculum_vitaes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cv` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `curriculum_vitaes`
--

INSERT INTO `curriculum_vitaes` (`id`, `cv`, `original_name`, `user_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1562520631Candidature-MQL.pdf', 'Candidature-MQL.pdf', 1, '2019-07-07 16:30:31', '2019-07-07 16:30:31', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `diplomas`
--

CREATE TABLE `diplomas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `year_of_graduation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cne` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `average` double DEFAULT NULL,
  `first_average` double DEFAULT NULL,
  `second_average` double DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entitled` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `year_of_registration` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number_year_stop` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `diplomas`
--

INSERT INTO `diplomas` (`id`, `user_id`, `year_of_graduation`, `country`, `region`, `city`, `cne`, `average`, `first_average`, `second_average`, `type`, `entitled`, `created_at`, `updated_at`, `deleted_at`, `year_of_registration`, `number_year_stop`) VALUES
(1, 1, '2012', 'Maroc', '9', 'Berkane', NULL, NULL, 12, 16, 'DTS', 'INFO', '2019-07-07 16:31:54', '2019-07-07 16:31:54', NULL, '2011', 0),
(4, 1, '2015', 'Maroc', '9', 'Berkane', '1127861544', 16, NULL, NULL, 'Baccalauréat', 'Sciences Physiques', '2019-07-07 16:41:12', '2019-07-07 16:41:12', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_03_17_233439_create_personal_informations_table', 1),
(4, '2018_03_17_233536_create_contact_informations_table', 1),
(5, '2018_03_17_233623_create_diplomas_table', 1),
(6, '2018_03_17_233758_create_prerequisites_table', 1),
(7, '2018_03_30_215845_create_curriculum_vitaes_table', 1),
(8, '2018_05_16_133837_add_culumn_is_admin_users', 1),
(9, '2018_05_16_234452_add_culumn_is_valid_users', 1),
(10, '2018_06_11_140431_create_roles_table', 1),
(11, '2018_06_11_142012_add_culumn_is_done_users', 1),
(12, '2018_06_11_172406_add_culumn_year_of_registration_diplomas', 1),
(13, '2018_06_12_092148_add_culumn_number_year_stop_diplomas', 1),
(14, '2018_06_27_093832_add_culumn_role_users', 1);

-- --------------------------------------------------------

--
-- Structure de la table `params`
--

CREATE TABLE `params` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `params`
--

INSERT INTO `params` (`id`, `name`, `value`) VALUES
(1, 'filiere', 'Ingénierie et Sécurité des Réseaux Informatiques'),
(2, 'abbreviation', 'LP-ISRI'),
(3, 'prerequis', 'Algorithmique,Programmation en C/C++,Architecture des ordinateurs,Système d’exploitation Linux'),
(4, 'avis', 'https://drive.google.com/open?id=0B2qwXfKzZFmmYmNuNG4tTzNIdjRoU29FNDhXa3lyWGFvbHFF'),
(5, 'email', 'omar.moussaoui78@gmail.com'),
(6, 'tel', '+212 36 500 224'),
(7, 'fix', '+212 36 500 224'),
(8, 'fax', '+212 36 500 223'),
(9, 'dossier', '<strong>Dossier de candidature</strong>\r\n<br>\r\n<ul>\r\n<li>Copie de l’accusé de préinscription en ligne</li>\r\n<li>Demande manuscrite adressée au Directeur de\r\nl’Ecole Supérieure de Technologie d’Oujda </li>\r\n<li>Copie du Baccalauréat </li>\r\n<li>Copie du diplôme bac + 2 (DUT, DEUG, ou\r\néquivalent) </li>\r\n<li>Copies des relevés des notes des deux années post\r\nbaccalauréat </li>\r\n<li>Une copie certifiée conforme de la C.I.N </li>\r\n<li>Curriculum Vitae</li>\r\n<li>Lettre de motivation</li>\r\n<li>Deux photos d’identité</li>\r\n<li>Deux enveloppes timbrées portant le nom et l’adresse du candidat.</li>\r\n</ul>');

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `pays`
--

CREATE TABLE `pays` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `code` int(3) NOT NULL,
  `alpha2` varchar(2) NOT NULL,
  `alpha3` varchar(3) NOT NULL,
  `nom_en_gb` varchar(45) NOT NULL,
  `nom_fr_fr` varchar(45) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pays`
--

INSERT INTO `pays` (`id`, `code`, `alpha2`, `alpha3`, `nom_en_gb`, `nom_fr_fr`) VALUES
(1, 4, 'AF', 'AFG', 'Afghanistan', 'Afghanistan'),
(2, 8, 'AL', 'ALB', 'Albania', 'Albanie'),
(3, 10, 'AQ', 'ATA', 'Antarctica', 'Antarctique'),
(4, 12, 'DZ', 'DZA', 'Algeria', 'Algérie'),
(5, 16, 'AS', 'ASM', 'American Samoa', 'Samoa Américaines'),
(6, 20, 'AD', 'AND', 'Andorra', 'Andorre'),
(7, 24, 'AO', 'AGO', 'Angola', 'Angola'),
(8, 28, 'AG', 'ATG', 'Antigua and Barbuda', 'Antigua-et-Barbuda'),
(9, 31, 'AZ', 'AZE', 'Azerbaijan', 'Azerbaïdjan'),
(10, 32, 'AR', 'ARG', 'Argentina', 'Argentine'),
(11, 36, 'AU', 'AUS', 'Australia', 'Australie'),
(12, 40, 'AT', 'AUT', 'Austria', 'Autriche'),
(13, 44, 'BS', 'BHS', 'Bahamas', 'Bahamas'),
(14, 48, 'BH', 'BHR', 'Bahrain', 'Bahreïn'),
(15, 50, 'BD', 'BGD', 'Bangladesh', 'Bangladesh'),
(16, 51, 'AM', 'ARM', 'Armenia', 'Arménie'),
(17, 52, 'BB', 'BRB', 'Barbados', 'Barbade'),
(18, 56, 'BE', 'BEL', 'Belgium', 'Belgique'),
(19, 60, 'BM', 'BMU', 'Bermuda', 'Bermudes'),
(20, 64, 'BT', 'BTN', 'Bhutan', 'Bhoutan'),
(21, 68, 'BO', 'BOL', 'Bolivia', 'Bolivie'),
(22, 70, 'BA', 'BIH', 'Bosnia and Herzegovina', 'Bosnie-Herzégovine'),
(23, 72, 'BW', 'BWA', 'Botswana', 'Botswana'),
(24, 74, 'BV', 'BVT', 'Bouvet Island', 'Île Bouvet'),
(25, 76, 'BR', 'BRA', 'Brazil', 'Brésil'),
(26, 84, 'BZ', 'BLZ', 'Belize', 'Belize'),
(27, 86, 'IO', 'IOT', 'British Indian Ocean Territory', 'Territoire Britannique de l''Océan Indien'),
(28, 90, 'SB', 'SLB', 'Solomon Islands', 'Îles Salomon'),
(29, 92, 'VG', 'VGB', 'British Virgin Islands', 'Îles Vierges Britanniques'),
(30, 96, 'BN', 'BRN', 'Brunei Darussalam', 'Brunéi Darussalam'),
(31, 100, 'BG', 'BGR', 'Bulgaria', 'Bulgarie'),
(32, 104, 'MM', 'MMR', 'Myanmar', 'Myanmar'),
(33, 108, 'BI', 'BDI', 'Burundi', 'Burundi'),
(34, 112, 'BY', 'BLR', 'Belarus', 'Bélarus'),
(35, 116, 'KH', 'KHM', 'Cambodia', 'Cambodge'),
(36, 120, 'CM', 'CMR', 'Cameroon', 'Cameroun'),
(37, 124, 'CA', 'CAN', 'Canada', 'Canada'),
(38, 132, 'CV', 'CPV', 'Cape Verde', 'Cap-vert'),
(39, 136, 'KY', 'CYM', 'Cayman Islands', 'Îles Caïmanes'),
(40, 140, 'CF', 'CAF', 'Central African', 'République Centrafricaine'),
(41, 144, 'LK', 'LKA', 'Sri Lanka', 'Sri Lanka'),
(42, 148, 'TD', 'TCD', 'Chad', 'Tchad'),
(43, 152, 'CL', 'CHL', 'Chile', 'Chili'),
(44, 156, 'CN', 'CHN', 'China', 'Chine'),
(45, 158, 'TW', 'TWN', 'Taiwan', 'Taïwan'),
(46, 162, 'CX', 'CXR', 'Christmas Island', 'Île Christmas'),
(47, 166, 'CC', 'CCK', 'Cocos (Keeling) Islands', 'Îles Cocos (Keeling)'),
(48, 170, 'CO', 'COL', 'Colombia', 'Colombie'),
(49, 174, 'KM', 'COM', 'Comoros', 'Comores'),
(50, 175, 'YT', 'MYT', 'Mayotte', 'Mayotte'),
(51, 178, 'CG', 'COG', 'Republic of the Congo', 'République du Congo'),
(52, 180, 'CD', 'COD', 'The Democratic Republic Of The Congo', 'République Démocratique du Congo'),
(53, 184, 'CK', 'COK', 'Cook Islands', 'Îles Cook'),
(54, 188, 'CR', 'CRI', 'Costa Rica', 'Costa Rica'),
(55, 191, 'HR', 'HRV', 'Croatia', 'Croatie'),
(56, 192, 'CU', 'CUB', 'Cuba', 'Cuba'),
(57, 196, 'CY', 'CYP', 'Cyprus', 'Chypre'),
(58, 203, 'CZ', 'CZE', 'Czech Republic', 'République Tchèque'),
(59, 204, 'BJ', 'BEN', 'Benin', 'Bénin'),
(60, 208, 'DK', 'DNK', 'Denmark', 'Danemark'),
(61, 212, 'DM', 'DMA', 'Dominica', 'Dominique'),
(62, 214, 'DO', 'DOM', 'Dominican Republic', 'République Dominicaine'),
(63, 218, 'EC', 'ECU', 'Ecuador', 'Équateur'),
(64, 222, 'SV', 'SLV', 'El Salvador', 'El Salvador'),
(65, 226, 'GQ', 'GNQ', 'Equatorial Guinea', 'Guinée Équatoriale'),
(66, 231, 'ET', 'ETH', 'Ethiopia', 'Éthiopie'),
(67, 232, 'ER', 'ERI', 'Eritrea', 'Érythrée'),
(68, 233, 'EE', 'EST', 'Estonia', 'Estonie'),
(69, 234, 'FO', 'FRO', 'Faroe Islands', 'Îles Féroé'),
(70, 238, 'FK', 'FLK', 'Falkland Islands', 'Îles (malvinas) Falkland'),
(71, 239, 'GS', 'SGS', 'South Georgia and the South Sandwich Islands', 'Géorgie du Sud et les Îles Sandwich du Sud'),
(72, 242, 'FJ', 'FJI', 'Fiji', 'Fidji'),
(73, 246, 'FI', 'FIN', 'Finland', 'Finlande'),
(74, 248, 'AX', 'ALA', 'Åland Islands', 'Îles Åland'),
(75, 250, 'FR', 'FRA', 'France', 'France'),
(76, 254, 'GF', 'GUF', 'French Guiana', 'Guyane Française'),
(77, 258, 'PF', 'PYF', 'French Polynesia', 'Polynésie Française'),
(78, 260, 'TF', 'ATF', 'French Southern Territories', 'Terres Australes Françaises'),
(79, 262, 'DJ', 'DJI', 'Djibouti', 'Djibouti'),
(80, 266, 'GA', 'GAB', 'Gabon', 'Gabon'),
(81, 268, 'GE', 'GEO', 'Georgia', 'Géorgie'),
(82, 270, 'GM', 'GMB', 'Gambia', 'Gambie'),
(83, 275, 'PS', 'PSE', 'Occupied Palestinian Territory', 'Territoire Palestinien Occupé'),
(84, 276, 'DE', 'DEU', 'Germany', 'Allemagne'),
(85, 288, 'GH', 'GHA', 'Ghana', 'Ghana'),
(86, 292, 'GI', 'GIB', 'Gibraltar', 'Gibraltar'),
(87, 296, 'KI', 'KIR', 'Kiribati', 'Kiribati'),
(88, 300, 'GR', 'GRC', 'Greece', 'Grèce'),
(89, 304, 'GL', 'GRL', 'Greenland', 'Groenland'),
(90, 308, 'GD', 'GRD', 'Grenada', 'Grenade'),
(91, 312, 'GP', 'GLP', 'Guadeloupe', 'Guadeloupe'),
(92, 316, 'GU', 'GUM', 'Guam', 'Guam'),
(93, 320, 'GT', 'GTM', 'Guatemala', 'Guatemala'),
(94, 324, 'GN', 'GIN', 'Guinea', 'Guinée'),
(95, 328, 'GY', 'GUY', 'Guyana', 'Guyana'),
(96, 332, 'HT', 'HTI', 'Haiti', 'Haïti'),
(97, 334, 'HM', 'HMD', 'Heard Island and McDonald Islands', 'Îles Heard et Mcdonald'),
(98, 336, 'VA', 'VAT', 'Vatican City State', 'Saint-Siège (état de la Cité du Vatican)'),
(99, 340, 'HN', 'HND', 'Honduras', 'Honduras'),
(100, 344, 'HK', 'HKG', 'Hong Kong', 'Hong-Kong'),
(101, 348, 'HU', 'HUN', 'Hungary', 'Hongrie'),
(102, 352, 'IS', 'ISL', 'Iceland', 'Islande'),
(103, 356, 'IN', 'IND', 'India', 'Inde'),
(104, 360, 'ID', 'IDN', 'Indonesia', 'Indonésie'),
(105, 364, 'IR', 'IRN', 'Islamic Republic of Iran', 'République Islamique d''Iran'),
(106, 368, 'IQ', 'IRQ', 'Iraq', 'Iraq'),
(107, 372, 'IE', 'IRL', 'Ireland', 'Irlande'),
(108, 376, 'IL', 'ISR', 'Israel', 'Israël'),
(109, 380, 'IT', 'ITA', 'Italy', 'Italie'),
(110, 384, 'CI', 'CIV', 'Côte d''Ivoire', 'Côte d''Ivoire'),
(111, 388, 'JM', 'JAM', 'Jamaica', 'Jamaïque'),
(112, 392, 'JP', 'JPN', 'Japan', 'Japon'),
(113, 398, 'KZ', 'KAZ', 'Kazakhstan', 'Kazakhstan'),
(114, 400, 'JO', 'JOR', 'Jordan', 'Jordanie'),
(115, 404, 'KE', 'KEN', 'Kenya', 'Kenya'),
(116, 408, 'KP', 'PRK', 'Democratic People''s Republic of Korea', 'République Populaire Démocratique de Corée'),
(117, 410, 'KR', 'KOR', 'Republic of Korea', 'République de Corée'),
(118, 414, 'KW', 'KWT', 'Kuwait', 'Koweït'),
(119, 417, 'KG', 'KGZ', 'Kyrgyzstan', 'Kirghizistan'),
(120, 418, 'LA', 'LAO', 'Lao People''s Democratic Republic', 'République Démocratique Populaire Lao'),
(121, 422, 'LB', 'LBN', 'Lebanon', 'Liban'),
(122, 426, 'LS', 'LSO', 'Lesotho', 'Lesotho'),
(123, 428, 'LV', 'LVA', 'Latvia', 'Lettonie'),
(124, 430, 'LR', 'LBR', 'Liberia', 'Libéria'),
(125, 434, 'LY', 'LBY', 'Libyan Arab Jamahiriya', 'Jamahiriya Arabe Libyenne'),
(126, 438, 'LI', 'LIE', 'Liechtenstein', 'Liechtenstein'),
(127, 440, 'LT', 'LTU', 'Lithuania', 'Lituanie'),
(128, 442, 'LU', 'LUX', 'Luxembourg', 'Luxembourg'),
(129, 446, 'MO', 'MAC', 'Macao', 'Macao'),
(130, 450, 'MG', 'MDG', 'Madagascar', 'Madagascar'),
(131, 454, 'MW', 'MWI', 'Malawi', 'Malawi'),
(132, 458, 'MY', 'MYS', 'Malaysia', 'Malaisie'),
(133, 462, 'MV', 'MDV', 'Maldives', 'Maldives'),
(134, 466, 'ML', 'MLI', 'Mali', 'Mali'),
(135, 470, 'MT', 'MLT', 'Malta', 'Malte'),
(136, 474, 'MQ', 'MTQ', 'Martinique', 'Martinique'),
(137, 478, 'MR', 'MRT', 'Mauritania', 'Mauritanie'),
(138, 480, 'MU', 'MUS', 'Mauritius', 'Maurice'),
(139, 484, 'MX', 'MEX', 'Mexico', 'Mexique'),
(140, 492, 'MC', 'MCO', 'Monaco', 'Monaco'),
(141, 496, 'MN', 'MNG', 'Mongolia', 'Mongolie'),
(142, 498, 'MD', 'MDA', 'Republic of Moldova', 'République de Moldova'),
(143, 500, 'MS', 'MSR', 'Montserrat', 'Montserrat'),
(144, 504, 'MA', 'MAR', 'Morocco', 'Maroc'),
(145, 508, 'MZ', 'MOZ', 'Mozambique', 'Mozambique'),
(146, 512, 'OM', 'OMN', 'Oman', 'Oman'),
(147, 516, 'NA', 'NAM', 'Namibia', 'Namibie'),
(148, 520, 'NR', 'NRU', 'Nauru', 'Nauru'),
(149, 524, 'NP', 'NPL', 'Nepal', 'Népal'),
(150, 528, 'NL', 'NLD', 'Netherlands', 'Pays-Bas'),
(151, 530, 'AN', 'ANT', 'Netherlands Antilles', 'Antilles Néerlandaises'),
(152, 533, 'AW', 'ABW', 'Aruba', 'Aruba'),
(153, 540, 'NC', 'NCL', 'New Caledonia', 'Nouvelle-Calédonie'),
(154, 548, 'VU', 'VUT', 'Vanuatu', 'Vanuatu'),
(155, 554, 'NZ', 'NZL', 'New Zealand', 'Nouvelle-Zélande'),
(156, 558, 'NI', 'NIC', 'Nicaragua', 'Nicaragua'),
(157, 562, 'NE', 'NER', 'Niger', 'Niger'),
(158, 566, 'NG', 'NGA', 'Nigeria', 'Nigéria'),
(159, 570, 'NU', 'NIU', 'Niue', 'Niué'),
(160, 574, 'NF', 'NFK', 'Norfolk Island', 'Île Norfolk'),
(161, 578, 'NO', 'NOR', 'Norway', 'Norvège'),
(162, 580, 'MP', 'MNP', 'Northern Mariana Islands', 'Îles Mariannes du Nord'),
(163, 581, 'UM', 'UMI', 'United States Minor Outlying Islands', 'Îles Mineures Éloignées des États-Unis'),
(164, 583, 'FM', 'FSM', 'Federated States of Micronesia', 'États Fédérés de Micronésie'),
(165, 584, 'MH', 'MHL', 'Marshall Islands', 'Îles Marshall'),
(166, 585, 'PW', 'PLW', 'Palau', 'Palaos'),
(167, 586, 'PK', 'PAK', 'Pakistan', 'Pakistan'),
(168, 591, 'PA', 'PAN', 'Panama', 'Panama'),
(169, 598, 'PG', 'PNG', 'Papua New Guinea', 'Papouasie-Nouvelle-Guinée'),
(170, 600, 'PY', 'PRY', 'Paraguay', 'Paraguay'),
(171, 604, 'PE', 'PER', 'Peru', 'Pérou'),
(172, 608, 'PH', 'PHL', 'Philippines', 'Philippines'),
(173, 612, 'PN', 'PCN', 'Pitcairn', 'Pitcairn'),
(174, 616, 'PL', 'POL', 'Poland', 'Pologne'),
(175, 620, 'PT', 'PRT', 'Portugal', 'Portugal'),
(176, 624, 'GW', 'GNB', 'Guinea-Bissau', 'Guinée-Bissau'),
(177, 626, 'TL', 'TLS', 'Timor-Leste', 'Timor-Leste'),
(178, 630, 'PR', 'PRI', 'Puerto Rico', 'Porto Rico'),
(179, 634, 'QA', 'QAT', 'Qatar', 'Qatar'),
(180, 638, 'RE', 'REU', 'Réunion', 'Réunion'),
(181, 642, 'RO', 'ROU', 'Romania', 'Roumanie'),
(182, 643, 'RU', 'RUS', 'Russian Federation', 'Fédération de Russie'),
(183, 646, 'RW', 'RWA', 'Rwanda', 'Rwanda'),
(184, 654, 'SH', 'SHN', 'Saint Helena', 'Sainte-Hélène'),
(185, 659, 'KN', 'KNA', 'Saint Kitts and Nevis', 'Saint-Kitts-et-Nevis'),
(186, 660, 'AI', 'AIA', 'Anguilla', 'Anguilla'),
(187, 662, 'LC', 'LCA', 'Saint Lucia', 'Sainte-Lucie'),
(188, 666, 'PM', 'SPM', 'Saint-Pierre and Miquelon', 'Saint-Pierre-et-Miquelon'),
(189, 670, 'VC', 'VCT', 'Saint Vincent and the Grenadines', 'Saint-Vincent-et-les Grenadines'),
(190, 674, 'SM', 'SMR', 'San Marino', 'Saint-Marin'),
(191, 678, 'ST', 'STP', 'Sao Tome and Principe', 'Sao Tomé-et-Principe'),
(192, 682, 'SA', 'SAU', 'Saudi Arabia', 'Arabie Saoudite'),
(193, 686, 'SN', 'SEN', 'Senegal', 'Sénégal'),
(194, 690, 'SC', 'SYC', 'Seychelles', 'Seychelles'),
(195, 694, 'SL', 'SLE', 'Sierra Leone', 'Sierra Leone'),
(196, 702, 'SG', 'SGP', 'Singapore', 'Singapour'),
(197, 703, 'SK', 'SVK', 'Slovakia', 'Slovaquie'),
(198, 704, 'VN', 'VNM', 'Vietnam', 'Viet Nam'),
(199, 705, 'SI', 'SVN', 'Slovenia', 'Slovénie'),
(200, 706, 'SO', 'SOM', 'Somalia', 'Somalie'),
(201, 710, 'ZA', 'ZAF', 'South Africa', 'Afrique du Sud'),
(202, 716, 'ZW', 'ZWE', 'Zimbabwe', 'Zimbabwe'),
(203, 724, 'ES', 'ESP', 'Spain', 'Espagne'),
(204, 732, 'EH', 'ESH', 'Western Sahara', 'Sahara Occidental'),
(205, 736, 'SD', 'SDN', 'Sudan', 'Soudan'),
(206, 740, 'SR', 'SUR', 'Suriname', 'Suriname'),
(207, 744, 'SJ', 'SJM', 'Svalbard and Jan Mayen', 'Svalbard etÎle Jan Mayen'),
(208, 748, 'SZ', 'SWZ', 'Swaziland', 'Swaziland'),
(209, 752, 'SE', 'SWE', 'Sweden', 'Suède'),
(210, 756, 'CH', 'CHE', 'Switzerland', 'Suisse'),
(211, 760, 'SY', 'SYR', 'Syrian Arab Republic', 'République Arabe Syrienne'),
(212, 762, 'TJ', 'TJK', 'Tajikistan', 'Tadjikistan'),
(213, 764, 'TH', 'THA', 'Thailand', 'Thaïlande'),
(214, 768, 'TG', 'TGO', 'Togo', 'Togo'),
(215, 772, 'TK', 'TKL', 'Tokelau', 'Tokelau'),
(216, 776, 'TO', 'TON', 'Tonga', 'Tonga'),
(217, 780, 'TT', 'TTO', 'Trinidad and Tobago', 'Trinité-et-Tobago'),
(218, 784, 'AE', 'ARE', 'United Arab Emirates', 'Émirats Arabes Unis'),
(219, 788, 'TN', 'TUN', 'Tunisia', 'Tunisie'),
(220, 792, 'TR', 'TUR', 'Turkey', 'Turquie'),
(221, 795, 'TM', 'TKM', 'Turkmenistan', 'Turkménistan'),
(222, 796, 'TC', 'TCA', 'Turks and Caicos Islands', 'Îles Turks et Caïques'),
(223, 798, 'TV', 'TUV', 'Tuvalu', 'Tuvalu'),
(224, 800, 'UG', 'UGA', 'Uganda', 'Ouganda'),
(225, 804, 'UA', 'UKR', 'Ukraine', 'Ukraine'),
(226, 807, 'MK', 'MKD', 'The Former Yugoslav Republic of Macedonia', 'L''ex-République Yougoslave de Macédoine'),
(227, 818, 'EG', 'EGY', 'Egypt', 'Égypte'),
(228, 826, 'GB', 'GBR', 'United Kingdom', 'Royaume-Uni'),
(229, 833, 'IM', 'IMN', 'Isle of Man', 'Île de Man'),
(230, 834, 'TZ', 'TZA', 'United Republic Of Tanzania', 'République-Unie de Tanzanie'),
(231, 840, 'US', 'USA', 'United States', 'États-Unis'),
(232, 850, 'VI', 'VIR', 'U.S. Virgin Islands', 'Îles Vierges des États-Unis'),
(233, 854, 'BF', 'BFA', 'Burkina Faso', 'Burkina Faso'),
(234, 858, 'UY', 'URY', 'Uruguay', 'Uruguay'),
(235, 860, 'UZ', 'UZB', 'Uzbekistan', 'Ouzbékistan'),
(236, 862, 'VE', 'VEN', 'Venezuela', 'Venezuela'),
(237, 876, 'WF', 'WLF', 'Wallis and Futuna', 'Wallis et Futuna'),
(238, 882, 'WS', 'WSM', 'Samoa', 'Samoa'),
(239, 887, 'YE', 'YEM', 'Yemen', 'Yémen'),
(240, 891, 'CS', 'SCG', 'Serbia and Montenegro', 'Serbie-et-Monténégro'),
(241, 894, 'ZM', 'ZMB', 'Zambia', 'Zambie');

-- --------------------------------------------------------

--
-- Structure de la table `personal_informations`
--

CREATE TABLE `personal_informations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'default.png',
  `family_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `native_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place_of_birth` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `personal_informations`
--

INSERT INTO `personal_informations` (`id`, `user_id`, `image`, `family_name`, `first_name`, `gender`, `birth_date`, `native_country`, `place_of_birth`, `type_id`, `id_number`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '1562520612video.png', 'YASSINE', 'EL', 'M', '24/03/1990', '144', 'Berkane', '1', 'FA153624', '2019-07-07 16:30:12', '2019-07-07 16:30:12', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `prerequisites`
--

CREATE TABLE `prerequisites` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `prerequisites_list` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `prerequisites`
--

INSERT INTO `prerequisites` (`id`, `user_id`, `prerequisites_list`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Algorithmique,Programmation en C/C++,Architecture des ordinateurs,Système d’exploitation Linux', '2019-07-07 16:25:48', '2019-07-07 16:25:48', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `region`
--

CREATE TABLE `region` (
  `id` int(11) NOT NULL,
  `region` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `region`
--

INSERT INTO `region` (`id`, `region`) VALUES
(1, 'Grand Casablanca'),
(2, 'Chaouia-Ouardigha'),
(3, 'Doukkala-Abda'),
(4, 'Fès-Boulemane'),
(5, 'Gharb-Chrarda-Beni Hssen'),
(6, 'Guelmim-Es Semara'),
(7, 'Marrakech-Tensift-Al Haouz'),
(8, 'Meknès-Tafilalet'),
(9, 'l''Oriental'),
(10, 'Rabat-Salé-Zemmour-Zaër'),
(11, 'Souss-Massa-Draâ'),
(12, 'Tadla-Azilal'),
(13, 'Tanger-Tétouan'),
(14, 'Taza-Al Hoceïma-Taounate'),
(15, 'Laayoune-Boujdour-Sakia-Hamra'),
(16, 'Oued-Eddahab-Lagouira');

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default_avatar.png',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `is_valid` tinyint(1) NOT NULL DEFAULT '0',
  `is_done` tinyint(1) NOT NULL DEFAULT '0',
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'candidat'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `avatar`, `remember_token`, `created_at`, `updated_at`, `deleted_at`, `is_admin`, `is_valid`, `is_done`, `role`) VALUES
(1, 'EL KANEB YASSINE', 'elkaneb.yassine@gmail.com', '$2y$10$/CY1vfV98.af7C2AJT3tkuPJBNgd2GENOBEkQu59VdVG.vNL6BoCO', '1562520612video.png', 'Ox51GdBNXHhVHUd3uxYSXfsOwt7XEqKxkxuHFhRpu5NMIKZdMCq88YvawSBd', '2019-07-07 16:25:03', '2019-07-07 16:42:32', NULL, 0, 0, 1, 'candidat');

-- --------------------------------------------------------

--
-- Structure de la table `ville`
--

CREATE TABLE `ville` (
  `id` int(11) NOT NULL,
  `ville` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `region` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `ville`
--

INSERT INTO `ville` (`id`, `ville`, `region`) VALUES
(1, 'Aïn Harrouda', 1),
(2, 'Ben Yakhlef', 1),
(3, 'Bouskoura', 1),
(4, 'Casablanca', 1),
(5, 'Médiouna', 1),
(6, 'Mohammédia', 1),
(7, 'Tit Mellil', 1),
(8, 'Ben Yakhlef', 2),
(9, 'Bejaâd', 2),
(10, 'Ben Ahmed', 2),
(11, 'Benslimane', 2),
(12, 'Berrechid', 2),
(13, 'Boujniba', 2),
(14, 'Boulanouare', 2),
(15, 'Bouznika', 2),
(16, 'Deroua', 2),
(17, 'El Borouj', 2),
(18, 'El Gara', 2),
(19, 'Guisser', 2),
(20, 'Hattane', 2),
(21, 'Khouribga', 2),
(22, 'Loulad', 2),
(23, 'Oued Zem', 2),
(24, 'Oulad Abbou', 2),
(25, 'Oulad H''Riz Sahel', 2),
(26, 'Oulad M''rah', 2),
(27, 'Oulad Saïd', 2),
(28, 'Oulad Sidi Ben Daoud', 2),
(29, 'Ras El Aïn', 2),
(30, 'Settat', 2),
(31, 'Sidi Rahhal Chataï', 2),
(32, 'Soualem', 2),
(33, 'Azemmour', 3),
(34, 'Bir Jdid', 3),
(35, 'Bouguedra', 3),
(36, 'Echemmaia', 3),
(37, 'El Jadida', 3),
(38, 'Hrara', 3),
(39, 'Ighoud', 3),
(40, 'Jamâat Shaim', 3),
(41, 'Jorf Lasfar', 3),
(42, 'Khemis Zemamra', 3),
(43, 'Laaounate', 3),
(44, 'Moulay Abdallah', 3),
(45, 'Oualidia', 3),
(46, 'Oulad Amrane', 3),
(47, 'Oulad Frej', 3),
(48, 'Oulad Ghadbane', 3),
(49, 'Safi', 3),
(50, 'Sebt El Maârif', 3),
(51, 'Sebt Gzoula', 3),
(52, 'Sidi Ahmed', 3),
(53, 'Sidi Ali Ban Hamdouche', 3),
(54, 'Sidi Bennour', 3),
(55, 'Sidi Bouzid', 3),
(56, 'Sidi Smaïl', 3),
(57, 'Youssoufia', 3),
(58, 'Fès', 4),
(59, 'Aïn Cheggag', 4),
(60, 'Bhalil', 4),
(61, 'Boulemane', 4),
(62, 'El Menzel', 4),
(63, 'Guigou', 4),
(64, 'Imouzzer Kandar', 4),
(65, 'Imouzzer Marmoucha', 4),
(66, 'Missour', 4),
(67, 'Moulay Yaâcoub', 4),
(68, 'Ouled Tayeb', 4),
(69, 'Outat El Haj', 4),
(70, 'Ribate El Kheir', 4),
(71, 'Séfrou', 4),
(72, 'Skhinate', 4),
(73, 'Tafajight', 4),
(74, 'Arbaoua', 5),
(75, 'Aïn Dorij', 5),
(76, 'Dar Gueddari', 5),
(77, 'Had Kourt', 5),
(78, 'Jorf El Melha', 5),
(79, 'Kénitra', 5),
(80, 'Khenichet', 5),
(81, 'Lalla Mimouna', 5),
(82, 'Mechra Bel Ksiri', 5),
(83, 'Mehdia', 5),
(84, 'Moulay Bousselham', 5),
(85, 'Sidi Allal Tazi', 5),
(86, 'Sidi Kacem', 5),
(87, 'Sidi Slimane', 5),
(88, 'Sidi Taibi', 5),
(89, 'Sidi Yahya El Gharb', 5),
(90, 'Souk El Arbaa', 5),
(91, 'Akka', 6),
(92, 'Assa', 6),
(93, 'Bouizakarne', 6),
(94, 'El Ouatia', 6),
(95, 'Es-Semara', 6),
(96, 'Fam El Hisn', 6),
(97, 'Foum Zguid', 6),
(98, 'Guelmim', 6),
(99, 'Taghjijt', 6),
(100, 'Tan-Tan', 6),
(101, 'Tata', 6),
(102, 'Zag', 6),
(103, 'Marrakech', 7),
(104, 'Ait Daoud', 7),
(115, 'Amizmiz', 7),
(116, 'Assahrij', 7),
(117, 'Aït Ourir', 7),
(118, 'Ben Guerir', 7),
(119, 'Chichaoua', 7),
(120, 'El Hanchane', 7),
(121, 'El Kelaâ des Sraghna', 7),
(122, 'Essaouira', 7),
(123, 'Fraïta', 7),
(124, 'Ghmate', 7),
(125, 'Ighounane', 7),
(126, 'Imintanoute', 7),
(127, 'Kattara', 7),
(128, 'Lalla Takerkoust', 7),
(129, 'Loudaya', 7),
(130, 'Lâattaouia', 7),
(131, 'Moulay Brahim', 7),
(132, 'Mzouda', 7),
(133, 'Ounagha', 7),
(134, 'Sid L''Mokhtar', 7),
(135, 'Sid Zouin', 7),
(136, 'Sidi Abdallah Ghiat', 7),
(137, 'Sidi Bou Othmane', 7),
(138, 'Sidi Rahhal', 7),
(139, 'Skhour Rehamna', 7),
(140, 'Smimou', 7),
(141, 'Tafetachte', 7),
(142, 'Tahannaout', 7),
(143, 'Talmest', 7),
(144, 'Tamallalt', 7),
(145, 'Tamanar', 7),
(146, 'Tamansourt', 7),
(147, 'Tameslouht', 7),
(148, 'Tanalt', 7),
(149, 'Zeubelemok', 7),
(150, 'Meknès‎', 8),
(151, 'Khénifra', 8),
(152, 'Agourai', 8),
(153, 'Ain Taoujdate', 8),
(154, 'MyAliCherif', 8),
(155, 'Rissani', 8),
(156, 'Amalou Ighriben', 8),
(157, 'Aoufous', 8),
(158, 'Arfoud', 8),
(159, 'Azrou', 8),
(160, 'Aïn Jemaa', 8),
(161, 'Aïn Karma', 8),
(162, 'Aïn Leuh', 8),
(163, 'Aït Boubidmane', 8),
(164, 'Aït Ishaq', 8),
(165, 'Boudnib', 8),
(166, 'Boufakrane', 8),
(167, 'Boumia', 8),
(168, 'El Hajeb', 8),
(169, 'Elkbab', 8),
(170, 'Er-Rich', 8),
(171, 'Errachidia', 8),
(172, 'Gardmit', 8),
(173, 'Goulmima', 8),
(174, 'Gourrama', 8),
(175, 'Had Bouhssoussen', 8),
(176, 'Haj Kaddour', 8),
(177, 'Ifrane', 8),
(178, 'Itzer', 8),
(179, 'Jorf', 8),
(180, 'Kehf Nsour', 8),
(181, 'Kerrouchen', 8),
(182, 'M''haya', 8),
(183, 'M''rirt', 8),
(184, 'Midelt', 8),
(185, 'Moulay Ali Cherif', 8),
(186, 'Moulay Bouazza', 8),
(187, 'Moulay Idriss Zerhoun', 8),
(188, 'Moussaoua', 8),
(189, 'N''Zalat Bni Amar', 8),
(190, 'Ouaoumana', 8),
(191, 'Oued Ifrane', 8),
(192, 'Sabaa Aiyoun', 8),
(193, 'Sebt Jahjouh', 8),
(194, 'Sidi Addi', 8),
(195, 'Tichoute', 8),
(196, 'Tighassaline', 8),
(197, 'Tighza', 8),
(198, 'Timahdite', 8),
(199, 'Tinejdad', 8),
(200, 'Tizguite', 8),
(201, 'Toulal', 8),
(202, 'Tounfite', 8),
(203, 'Zaouia d''Ifrane', 8),
(204, 'Zaïda', 8),
(205, 'Ahfir', 9),
(206, 'Aklim', 9),
(207, 'Al Aroui', 9),
(208, 'Aïn Bni Mathar', 9),
(209, 'Aïn Erreggada', 9),
(210, 'Ben Taïeb', 9),
(211, 'Berkane', 9),
(212, 'Bni Ansar', 9),
(213, 'Bni Chiker', 9),
(214, 'Bni Drar', 9),
(215, 'Bni Tadjite', 9),
(216, 'Bouanane', 9),
(217, 'Bouarfa', 9),
(218, 'Bouhdila', 9),
(219, 'Dar El Kebdani', 9),
(220, 'Debdou', 9),
(221, 'Douar Kannine', 9),
(222, 'Driouch', 9),
(223, 'El Aïoun Sidi Mellouk', 9),
(224, 'Farkhana', 9),
(225, 'Figuig', 9),
(226, 'Ihddaden', 9),
(227, 'Jaâdar', 9),
(228, 'Jerada', 9),
(229, 'Kariat Arekmane', 9),
(230, 'Kassita', 9),
(231, 'Kerouna', 9),
(232, 'Laâtamna', 9),
(233, 'Madagh', 9),
(234, 'Midar', 9),
(235, 'Nador', 9),
(236, 'Naima', 9),
(237, 'Oued Heimer', 9),
(238, 'Oujda', 9),
(239, 'Ras El Ma', 9),
(240, 'Saïdia', 9),
(241, 'Selouane', 9),
(242, 'Sidi Boubker', 9),
(243, 'Sidi Slimane Echcharaa', 9),
(244, 'Talsint', 9),
(245, 'Taourirt', 9),
(246, 'Tendrara', 9),
(247, 'Tiztoutine', 9),
(248, 'Touima', 9),
(249, 'Touissit', 9),
(250, 'Zaïo', 9),
(251, 'Zeghanghane', 9),
(252, 'Rabat', 10),
(253, 'Salé', 10),
(254, 'Ain El Aouda', 10),
(255, 'Harhoura', 10),
(256, 'Khémisset', 10),
(257, 'Oulmès', 10),
(258, 'Rommani', 10),
(259, 'Sidi Allal El Bahraoui', 10),
(260, 'Sidi Bouknadel', 10),
(261, 'Skhirat', 10),
(262, 'Tamesna', 10),
(263, 'Témara', 10),
(264, 'Tiddas', 10),
(265, 'Tiflet', 10),
(266, 'Touarga', 10),
(267, 'Agadir', 11),
(268, 'Agdz', 11),
(269, 'Agni Izimmer', 11),
(270, 'Aït Melloul', 11),
(271, 'Alnif', 11),
(272, 'Anzi', 11),
(273, 'Aoulouz', 11),
(274, 'Aourir', 11),
(275, 'Arazane', 11),
(276, 'Aït Baha', 11),
(277, 'Aït Iaâza', 11),
(278, 'Aït Yalla', 11),
(279, 'Ben Sergao', 11),
(280, 'Biougra', 11),
(281, 'Boumalne-Dadès', 11),
(282, 'Dcheira El Jihadia', 11),
(283, 'Drargua', 11),
(284, 'El Guerdane', 11),
(285, 'Harte Lyamine', 11),
(286, 'Ida Ougnidif', 11),
(287, 'Ifri', 11),
(288, 'Igdamen', 11),
(289, 'Ighil n''Oumgoun', 11),
(290, 'Imassine', 11),
(291, 'Inezgane', 11),
(292, 'Irherm', 11),
(293, 'Kelaat-M''Gouna', 11),
(294, 'Lakhsas', 11),
(295, 'Lakhsass', 11),
(296, 'Lqliâa', 11),
(297, 'M''semrir', 11),
(298, 'Massa (Maroc)', 11),
(299, 'Megousse', 11),
(300, 'Ouarzazate', 11),
(301, 'Oulad Berhil', 11),
(302, 'Oulad Teïma', 11),
(303, 'Sarghine', 11),
(304, 'Sidi Ifni', 11),
(305, 'Skoura', 11),
(306, 'Tabounte', 11),
(307, 'Tafraout', 11),
(308, 'Taghzout', 11),
(309, 'Tagzen', 11),
(310, 'Taliouine', 11),
(311, 'Tamegroute', 11),
(312, 'Tamraght', 11),
(313, 'Tanoumrite Nkob Zagora', 11),
(314, 'Taourirt ait zaghar', 11),
(315, 'Taroudant', 11),
(316, 'Temsia', 11),
(317, 'Tifnit', 11),
(318, 'Tisgdal', 11),
(319, 'Tiznit', 11),
(320, 'Toundoute', 11),
(321, 'Zagora', 11),
(322, 'Afourar', 12),
(323, 'Aghbala', 12),
(324, 'Azilal', 12),
(325, 'Aït Majden', 12),
(326, 'Beni Ayat', 12),
(327, 'Béni Mellal', 12),
(328, 'Bin elouidane', 12),
(329, 'Bradia', 12),
(330, 'Bzou', 12),
(331, 'Dar Oulad Zidouh', 12),
(332, 'Demnate', 12),
(333, 'Dra''a', 12),
(334, 'El Ksiba', 12),
(335, 'Foum Jamaa', 12),
(336, 'Fquih Ben Salah', 12),
(337, 'Kasba Tadla', 12),
(338, 'Ouaouizeght', 12),
(339, 'Oulad Ayad', 12),
(340, 'Oulad M''Barek', 12),
(341, 'Oulad Yaich', 12),
(342, 'Sidi Jaber', 12),
(343, 'Souk Sebt Oulad Nemma', 12),
(344, 'Zaouïat Cheikh', 12),
(345, 'Tanger‎', 13),
(346, 'Tétouan‎', 13),
(347, 'Akchour', 13),
(348, 'Assilah', 13),
(349, 'Bab Berred', 13),
(350, 'Bab Taza', 13),
(351, 'Brikcha', 13),
(352, 'Chefchaouen', 13),
(353, 'Dar Bni Karrich', 13),
(354, 'Dar Chaoui', 13),
(355, 'Fnideq', 13),
(356, 'Gueznaia', 13),
(357, 'Jebha', 13),
(358, 'Karia', 13),
(359, 'Khémis Sahel', 13),
(360, 'Ksar El Kébir', 13),
(361, 'Larache', 13),
(362, 'M''diq', 13),
(363, 'Martil', 13),
(364, 'Moqrisset', 13),
(365, 'Oued Laou', 13),
(366, 'Oued Rmel', 13),
(367, 'Ouezzane', 13),
(368, 'Point Cires', 13),
(369, 'Sidi Lyamani', 13),
(370, 'Sidi Mohamed ben Abdallah el-Raisuni', 13),
(371, 'Zinat', 13),
(372, 'Ajdir‎', 14),
(373, 'Aknoul‎', 14),
(374, 'Al Hoceïma‎', 14),
(375, 'Aït Hichem‎', 14),
(376, 'Bni Bouayach‎', 14),
(377, 'Bni Hadifa‎', 14),
(378, 'Ghafsai‎', 14),
(379, 'Guercif‎', 14),
(380, 'Imzouren‎', 14),
(381, 'Inahnahen‎', 14),
(382, 'Issaguen (Ketama)‎', 14),
(383, 'Karia (El Jadida)‎', 14),
(384, 'Karia Ba Mohamed‎', 14),
(385, 'Oued Amlil‎', 14),
(386, 'Oulad Zbair‎', 14),
(387, 'Tahla‎', 14),
(388, 'Tala Tazegwaght‎', 14),
(389, 'Tamassint‎', 14),
(390, 'Taounate‎', 14),
(391, 'Targuist‎', 14),
(392, 'Taza‎', 14),
(393, 'Taïnaste‎', 14),
(394, 'Thar Es-Souk‎', 14),
(395, 'Tissa‎', 14),
(396, 'Tizi Ouasli‎', 14),
(397, 'Laayoune‎', 15),
(398, 'El Marsa‎', 15),
(399, 'Tarfaya‎', 15),
(400, 'Boujdour‎', 15),
(401, 'Awsard', 16),
(402, 'Oued-Eddahab ', 16),
(403, 'Stehat', 13),
(404, 'Aït Attab', 12);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `contact_informations`
--
ALTER TABLE `contact_informations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contact_informations_user_id_index` (`user_id`);

--
-- Index pour la table `curriculum_vitaes`
--
ALTER TABLE `curriculum_vitaes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `curriculum_vitaes_user_id_index` (`user_id`);

--
-- Index pour la table `diplomas`
--
ALTER TABLE `diplomas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `diplomas_user_id_index` (`user_id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `params`
--
ALTER TABLE `params`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Index pour la table `pays`
--
ALTER TABLE `pays`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `alpha2` (`alpha2`),
  ADD UNIQUE KEY `alpha3` (`alpha3`),
  ADD UNIQUE KEY `code_unique` (`code`);

--
-- Index pour la table `personal_informations`
--
ALTER TABLE `personal_informations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `personal_informations_user_id_index` (`user_id`);

--
-- Index pour la table `prerequisites`
--
ALTER TABLE `prerequisites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `prerequisites_user_id_index` (`user_id`);

--
-- Index pour la table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roles_user_id_index` (`user_id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Index pour la table `ville`
--
ALTER TABLE `ville`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `contact_informations`
--
ALTER TABLE `contact_informations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `curriculum_vitaes`
--
ALTER TABLE `curriculum_vitaes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `diplomas`
--
ALTER TABLE `diplomas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `params`
--
ALTER TABLE `params`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `pays`
--
ALTER TABLE `pays`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=242;
--
-- AUTO_INCREMENT pour la table `personal_informations`
--
ALTER TABLE `personal_informations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `prerequisites`
--
ALTER TABLE `prerequisites`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `region`
--
ALTER TABLE `region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `ville`
--
ALTER TABLE `ville`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=405;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `contact_informations`
--
ALTER TABLE `contact_informations`
  ADD CONSTRAINT `contact_informations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `curriculum_vitaes`
--
ALTER TABLE `curriculum_vitaes`
  ADD CONSTRAINT `curriculum_vitaes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `diplomas`
--
ALTER TABLE `diplomas`
  ADD CONSTRAINT `diplomas_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `personal_informations`
--
ALTER TABLE `personal_informations`
  ADD CONSTRAINT `personal_informations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `prerequisites`
--
ALTER TABLE `prerequisites`
  ADD CONSTRAINT `prerequisites_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
