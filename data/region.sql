-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Dim 07 Juillet 2019 à 18:49
-- Version du serveur :  10.1.13-MariaDB
-- Version de PHP :  5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ESTOAPP`
--

-- --------------------------------------------------------

--
-- Structure de la table `region`
--

CREATE TABLE `region` (
  `id` int(11) NOT NULL,
  `region` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `region`
--

INSERT INTO `region` (`id`, `region`) VALUES
(1, 'Grand Casablanca'),
(2, 'Chaouia-Ouardigha'),
(3, 'Doukkala-Abda'),
(4, 'Fès-Boulemane'),
(5, 'Gharb-Chrarda-Beni Hssen'),
(6, 'Guelmim-Es Semara'),
(7, 'Marrakech-Tensift-Al Haouz'),
(8, 'Meknès-Tafilalet'),
(9, 'l''Oriental'),
(10, 'Rabat-Salé-Zemmour-Zaër'),
(11, 'Souss-Massa-Draâ'),
(12, 'Tadla-Azilal'),
(13, 'Tanger-Tétouan'),
(14, 'Taza-Al Hoceïma-Taounate'),
(15, 'Laayoune-Boujdour-Sakia-Hamra'),
(16, 'Oued-Eddahab-Lagouira');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `region`
--
ALTER TABLE `region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
