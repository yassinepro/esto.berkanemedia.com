-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Dim 07 Juillet 2019 à 21:08
-- Version du serveur :  10.1.13-MariaDB
-- Version de PHP :  5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `esto_test_2019`
--

-- --------------------------------------------------------

--
-- Structure de la table `params`
--

CREATE TABLE `params` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `params`
--

INSERT INTO `params` (`id`, `name`, `value`) VALUES
(1, 'filiere', 'Ingénierie et Sécurité des Réseaux Informatiques'),
(2, 'abbreviation', 'LP-ISRI'),
(3, 'prerequis', 'Algorithmique,Programmation en C/C++,Architecture des ordinateurs,Système d’exploitation Linux'),
(4, 'avis', 'https://drive.google.com/open?id=0B2qwXfKzZFmmYmNuNG4tTzNIdjRoU29FNDhXa3lyWGFvbHFF'),
(5, 'email', 'omar.moussaoui78@gmail.com'),
(6, 'tel', '+212 36 500 224'),
(7, 'fix', '+212 36 500 224'),
(8, 'fax', '+212 36 500 223'),
(9, 'dossier', '<strong>Dossier de candidature</strong>\r\n<br>\r\n<ul>\r\n<li>Copie de l’accusé de préinscription en ligne</li>\r\n<li>Demande manuscrite adressée au Directeur de\r\nl’Ecole Supérieure de Technologie d’Oujda </li>\r\n<li>Copie du Baccalauréat </li>\r\n<li>Copie du diplôme bac + 2 (DUT, DEUG, ou\r\néquivalent) </li>\r\n<li>Copies des relevés des notes des deux années post\r\nbaccalauréat </li>\r\n<li>Une copie certifiée conforme de la C.I.N </li>\r\n<li>Curriculum Vitae</li>\r\n<li>Lettre de motivation</li>\r\n<li>Deux photos d’identité</li>\r\n<li>Deux enveloppes timbrées portant le nom et l’adresse du candidat.</li>\r\n</ul>');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `params`
--
ALTER TABLE `params`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `params`
--
ALTER TABLE `params`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
