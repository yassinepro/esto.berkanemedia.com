<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     if(\Auth::user())
//     {
//         return  redirect('home');
//     } else{
        
//         return view('welcome');
//     }
// })->name('welcome');

Route::get('/', 'WelcomeController@index')->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/validation', 'HomeController@validation')->name('validation');
Route::post('/dossiervalide', 'HomeController@dossiervalide')->name('dossiervalide');
Route::get('/export', 'HomeController@export')->name('export');
Route::post('/pdf', 'HomeController@pdf')->name('pdf');
Route::post('/api/villes', 'HomeController@villes')->name('villes');
Route::post('/dossier', 'HomeController@dossier')->name('dossier');

Route::resource('diploma','DiplomaController');
Route::resource('information','PersonalInformationController');
Route::resource('contact','PersonalInformationController');
Route::resource('cv','CurriculumVitaeController');
Route::resource('prerequisite','PrerequisiteController');
