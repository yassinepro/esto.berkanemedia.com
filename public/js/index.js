jQuery(function($) {
	"use strict";

	// Window Load Function
	$(window).load(function() {
		
	    // Preloader
	    $(".preloader").fadeOut();

	});

	// Slides
	$('#slides').superslides({
		play: 4000,
		animation_speed: 2000,
		pagination: true,
	});

		// Toggle Sidebar
	$('.toggle-sidebar').on('click', function() {
		$('.sidebar').toggleClass('content-hidden');
	});

	// Open Newsletter Page 
	$('.newsletter-button').on('click', function() {
		//$('#newsletter').toggleClass('in');
	});

	// Open Contact Page 
	$('.contact-button').on('click', function() {
		$('#contact').toggleClass('in');
	});

	// Close Page
	$('.close').on('click', function() {
		$('.page').removeClass('in');
	});

	// Newsletter Form
	$('#newsletter-form').ajaxForm(function() { 
		$(this).addClass('out');
	    $('.newsletter-form-success').addClass('in');
	});

});