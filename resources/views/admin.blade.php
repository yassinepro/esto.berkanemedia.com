@extends('layouts.dashboard')

@section('title', 'ESTO | Ecole Supérieure de Technologie - Oujda')

@section('breadcrumb', 'Dashboard')

@push('styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush
@section('content')
<!-- Info boxes -->
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="ion ion-ios-people-outline"></i></span>

        <div class="info-box-content">
            <span class="info-box-text">DUT</span>
            <span class="info-box-number">{{ $total_dut }}<!--<small>%</small>--></span>
        </div>
        <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa ion-ios-people-outline"></i></span>

        <div class="info-box-content">
            <span class="info-box-text">DEUP</span>
            <span class="info-box-number">{{ $total_deup }}<!--<small>%</small>--></span>
        </div>
        <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-green"><i class="ion ion-ios-people-outline"></i></span>

        <div class="info-box-content">
            <span class="info-box-text">Autre</span>
            <span class="info-box-number">{{ $total_autre }}<!--<small>%</small>--></span>
        </div>
        <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

        <div class="info-box-content">
            <span class="info-box-text">candidats</span>
            <span class="info-box-number">{{ $total_members }}</span>
        </div>
        <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->


<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Tous les candidats
          <a href="{{ route('export') }}" class="btn btn-xs btn-success">
            Exporter
          </a>
        </h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="example3" class="table table-bordered table-hover">
          <thead>
          <tr>
            <th>#Numéro</th>
            <th>Nom et Prénom</th>
            <th>E-mail</th>
            <th>Date d'inscription</th>
            <th>Etat</th>
          </tr>
          </thead>
          <tbody>
          @foreach($users->unique('id') as $user)
          <tr>
            <td>{{ $user->id}} </td>
            <td>{{ title_case($user->name) }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->created_at }}</td>
            <td>
                @if ($user->is_valid == 1)
                <i class="fa fa-check text-success"></i> 
                @else
                  <i class="fa fa-close text-danger"></i> 
                @endif
            </td>
          </tr>
          @endforeach
          </tbody>
          <!-- <tfoot>
          <tr>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Date de naissance</th>
            <th>Téléphone</th>
          </tr>
          </tfoot> -->
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
@endsection

@push('scripts')
<!-- DataTables -->
<script src="{{ asset('components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script>
  $(function () {
    $('#example3').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false,
      "columnDefs": [
                    { "targets": [3,4], "searchable": false }
                ]
    });
  });  
</script>
@endpush