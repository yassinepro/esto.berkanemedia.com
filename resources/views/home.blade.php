@extends('layouts.apply',['personalInformation'=>$personalInformation])

@section('title', 'ESTO | Ecole Supérieure de Technologie - Oujda')

@section('breadcrumb', 'Mon dossier')

@push('styles')
  <style>
    label {
      font-weight: 400 !important;
    }
  </style>
@endpush
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Mon dossier</h3>
      </div>
      <!-- /.box-header -->
        <div class="box-body">
          <div class="box-group" id="accordion">
            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
            <div class="panel box box-primary">
              <div class="box-header with-border">
                <h4 class="box-title">
                  @if($personalInformation)
                    <a href="{{ route('information.index') }}">
                      Mes Informations Personnelles #1 
                      <small class="text-green">complet</small>
                    </a>
                  @else
                    <a href="{{ route('information.index') }}">
                      Mes Informations Personnelles #1
                      <small class="text-red">Incomplet</small> 
                    </a>
                  @endif
                </h4>
              </div>
            </div>
            <div class="panel box box-danger">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a href="{{ route('diploma.index') }}">
                    Mon parcours et mes diplômes #2 
                    @if($diploma < 2 or $cv == 0 )
                      <small class="text-red">Incomplet</small>
                    @else
                      <small class="text-green">complet</small>
                    @endif
                  </a>
                </h4>
              </div>
            </div>
            <div class="panel box box-success">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                    Prérequis & Validation #3
                    @if($prerequisites )
                      <small class="text-green">complet</small>
                    @else
                      <small class="text-red">Incomplet</small>
                    @endif
                  </a>
                </h4>
              </div>
              <div id="collapseThree" class="panel-collapse collapse in">
                <div class="box-body">
                  <div class="col-md-6">
                      @foreach ($prerequis as $item)
                        <input @if(in_array($item, $prerequisites)) checked @endif id="item{{ $loop->index }}" type="checkbox" value="{{ $item }}" name="prerequisites_list[]"> <label for="item{{ $loop->index }}">{{ $item }}</label> <br>
                      @endforeach
                  </div>
                  <div class="col-md-6">
                    <p style="font-weight:bold;" class="text-danger">Attention, après la soumission de votre dossier, vous ne pourrez plus le modifier !</p>
                    <form role="form" action="{{ route('dossier') }}" method="post">
                      @csrf
                      <!-- <p>
                        <input name="certify" id="certify" type="checkbox">
                        <label style="display:inline;" for="certify">J'ai bien relu mon dossier et je certifie l'exactitude des informations qu'il contient</label>
                      </p>   -->
                      <button style="display:none;" id="prerequisite" type="button" class="btn btn-primary">Enregistrer</button>

                      <button id="prerequisite_disabled" type="button" class="btn btn-primary disabled">Enregistrer</button>

                      <button id="certify-btn" type="button" class="btn btn-danger disabled">Je soumets mon dossier</button>
                      @php
                        $dossier_pas_complet = true
                      @endphp
                      @if($prerequisites)
                        @if($personalInformation)
                          @if($diploma >= 2 and $cv != 0)
                            @php
                              $dossier_pas_complet = false
                            @endphp
                          @endif
                        @endif
                      @endif

                      @if($dossier_pas_complet)
                        <p class="help-block text-danger">
                          Votre dossier n'est pas complet. Vous ne pouvez pas le soumettre.
                        </p>
                      @endif
        
                    </form>  
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
  <div class="col-md-12">
    
  </div>
</div>
@endsection

@push('scripts')
<!-- InputMask -->
<script src="{{ asset('js/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('js/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('js/input-mask/jquery.inputmask.extensions.js') }}"></script>

<script src="{{ asset('js/intl-tel-input/js/intlTelInput.js') }}"></script>
<script>
  $("#phone").intlTelInput();
</script>
<script>
  $(function() {
    //Datemask dd/mm/yyyy
    $('#datemask, #datemask1').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
  });
</script>
<script>
    $(function(){



        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

        @if($prerequisites)
          @if($personalInformation)
            @if($diploma >= 2 and $cv != 0)
              //$('#certify').change(function(){
                  //if($('#certify').is(':checked') == true){
                      $('#certify-btn').removeClass('disabled');
                      $('#certify-btn').attr('type','submit');
                      $('#certify-btn').attr('onclick','return true;');
                  //}else{
                    //  $('#certify-btn').addClass('disabled');
                    //  $('#certify-btn').attr('onclick','return false;');
                  //}
              //});
            @endif
          @endif
        @endif

        $("input[name*='pre']").change(function(){
            $('#prerequisite_disabled').hide();
            $('#prerequisite').show();
        });

        $('#prerequisite').click(function(){
            //alert($("input[name*='prerequisites_list']"));

            var data = { 'prerequisites_list[]' : []};
            $("input[name*='pre']:checked").each(function() {
              data['prerequisites_list[]'].push($(this).val());
            });
            $.post("{{ route('prerequisite.store')}}", data,
            function(data){
                    window.location.reload();
            });
        });

    });
</script>
@endpush