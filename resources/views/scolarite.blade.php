@extends('layouts.dashboard')

@section('title', 'ESTO | Ecole Supérieure de Technologie - Oujda')

@section('breadcrumb', 'Dashboard')

@push('styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush
@section('content')
<!-- Info boxes -->
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="ion ion-ios-people-outline"></i></span>

        <div class="info-box-content">
            <span class="info-box-text">DUT</span>
            <span class="info-box-number">{{ $total_dut }}<!--<small>%</small>--></span>
        </div>
        <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa ion-ios-people-outline"></i></span>

        <div class="info-box-content">
            <span class="info-box-text">DEUP</span>
            <span class="info-box-number">{{ $total_deup }}<!--<small>%</small>--></span>
        </div>
        <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-green"><i class="ion ion-ios-people-outline"></i></span>

        <div class="info-box-content">
            <span class="info-box-text">Autre</span>
            <span class="info-box-number">{{ $total_autre }}<!--<small>%</small>--></span>
        </div>
        <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

        <div class="info-box-content">
            <span class="info-box-text">candidats</span>
            <span class="info-box-number">{{ $total_members }}</span>
        </div>
        <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->


<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Tous les candidats
          <a href="{{ route('export') }}" class="btn btn-xs btn-success">
            Exporter
          </a>
        </h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="example2" class="table table-bordered table-hover">
          <thead>
          <tr>
            <th>#Numéro</th>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Date de naissance</th>
            <th>Téléphone</th>
            <th>Etat</th>
          </tr>
          </thead>
          <tbody>
          @foreach($users->unique('user_id') as $user)
          <tr>
            <td>{{ $user->user_id}} </td>
            <td>{{ title_case($user->family_name) }}</td>
            <td>{{ title_case($user->first_name) }}</td>
            <td>{{ $user->birth_date }}</td>
            <td>{{ $user->phone }}</td>
            <td>
                @if ($user->is_valid == 1)
                  <button class="btn btn-xs btn-success disabled">validé</button>
                @else
                  <form action="{{ route('validation') }}" method="POST">
                      @csrf
                      <button type="submit" class="btn btn-xs btn-danger">
                        validé
                      </button>

                      <input type="hidden" value="{{ $user->user_id }}" name="code">
                  </form>
                @endif
            </td>
          </tr>
          @endforeach
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
@endsection

@push('scripts')
<!-- DataTables -->
<script src="{{ asset('components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('components/chart.js/Chart.js') }}"></script>
<script>
  $(function () {
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false,
      "columnDefs": [
                    { "targets": [3,4,5], "searchable": false }
                ]
    });



      // -------------
  // - PIE CHART -
  // -------------
  // Get context with jQuery - using jQuery's .get() method.
  var pieChartCanvas = $('#pieChart').get(0).getContext('2d');
  var pieChart       = new Chart(pieChartCanvas);
  var PieData        = [
    {
      value    : {{ $total_dut }},
      color    : '#f56954',
      highlight: '#f56954',
      label    : 'DUT'
    },
    {
      value    : {{ $total_deup }},
      color    : '#00a65a',
      highlight: '#00a65a',
      label    : 'DEUP'
    },
    {
      value    : {{ $total_autre }},
      color    : '#f39c12',
      highlight: '#f39c12',
      label    : 'DTS'
    },
    {
      value    : {{ $total_autre }},
      color    : '#00c0ef',
      highlight: '#00c0ef',
      label    : 'BTS'
    }
  ];
  var pieOptions     = {
    // Boolean - Whether we should show a stroke on each segment
    segmentShowStroke    : true,
    // String - The colour of each segment stroke
    segmentStrokeColor   : '#fff',
    // Number - The width of each segment stroke
    segmentStrokeWidth   : 1,
    // Number - The percentage of the chart that we cut out of the middle
    percentageInnerCutout: 50, // This is 0 for Pie charts
    // Number - Amount of animation steps
    animationSteps       : 100,
    // String - Animation easing effect
    animationEasing      : 'easeOutBounce',
    // Boolean - Whether we animate the rotation of the Doughnut
    animateRotate        : true,
    // Boolean - Whether we animate scaling the Doughnut from the centre
    animateScale         : false,
    // Boolean - whether to make the chart responsive to window resizing
    responsive           : true,
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio  : false,
    // String - A legend template
    legendTemplate       : '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<segments.length; i++){%><li><span style=\'background-color:<%=segments[i].fillColor%>\'></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
    // String - A tooltip template
    tooltipTemplate      : '<%=value %> <%=label%>'
  };
  // Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  pieChart.Doughnut(PieData, pieOptions);
  // -----------------
  // - END PIE CHART -
  // -----------------

  });



</script>
@endpush