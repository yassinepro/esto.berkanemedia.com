@extends('layouts.dashboard')

@section('title', 'ESTO | Ecole Supérieure de Technologie - Oujda')

@section('breadcrumb', 'Dashboard')

@push('styles')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush
@section('content')
<!-- Info boxes -->
<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-aqua"><i class="ion ion-ios-people-outline"></i></span>

        <div class="info-box-content">
            <span class="info-box-text">DUT</span>
            <span class="info-box-number">{{ $total_dut }}<!--<small>%</small>--></span>
        </div>
        <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-red"><i class="fa ion-ios-people-outline"></i></span>

        <div class="info-box-content">
            <span class="info-box-text">DEUP</span>
            <span class="info-box-number">{{ $total_deup }}<!--<small>%</small>--></span>
        </div>
        <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->

    <!-- fix for small devices only -->
    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-green"><i class="ion ion-ios-people-outline"></i></span>

        <div class="info-box-content">
            <span class="info-box-text">Autre</span>
            <span class="info-box-number">{{ $total_autre }}<!--<small>%</small>--></span>
        </div>
        <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
        <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

        <div class="info-box-content">
            <span class="info-box-text">candidats</span>
            <span class="info-box-number">{{ $total_members }}</span>
        </div>
        <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->


<div class="row">
  <div class="col-xs-12">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Tous les candidats
          <a href="{{ route('export') }}" class="btn btn-xs btn-success">
            Exporter
          </a>
        </h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="example2" class="table table-bordered table-hover">
          <thead>
          <tr>
            <th>#Numéro</th>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Date de naissance</th>
            <th>Téléphone</th>
          </tr>
          </thead>
          <tbody>
          @foreach($users->unique('user_id') as $user)
          <tr>
            <td>{{ $user->user_id}} </td>
            <td>{{ title_case($user->family_name) }}</td>
            <td>{{ title_case($user->first_name) }}</td>
            <td>{{ $user->birth_date }}</td>
            <td>{{ $user->phone }}</td>
          </tr>
          @endforeach
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->

<div class="row">
  <div class="col-md-8">
    <!-- TABLE: LATEST ORDERS -->
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Statistique</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="table-responsive">
          
        </div>
        <!-- /.table-responsive -->
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <div class="col-md-4">
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">% des candidats par type</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
          <div class="col-md-8">
            <div class="chart-responsive">
              <canvas id="pieChart" height="150"></canvas>
            </div>
            <!-- ./chart-responsive -->
          </div>
          <!-- /.col -->
          <div class="col-md-4">
            <ul class="chart-legend clearfix">
              <li><i class="fa fa-circle-o text-red"></i> DUT</li>
              <li><i class="fa fa-circle-o text-green"></i> DEUP</li>
              <li><i class="fa fa-circle-o text-yellow"></i> AUTRE</li>
            </ul>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.box-body -->
      <div class="box-footer no-padding">
        <ul class="nav nav-pills nav-stacked">
          @if ($total_members == 0)
          <li>
            <a href="#">DUT
              <span class="pull-right text-red"> 0%</span>
            </a>
          </li>
          <li>
            <a href="#">DEUP
              <span class="pull-right text-green"> 0%</span>
            </a>
          </li>
          <li>
            <a href="#">AUTRE
              <span class="pull-right text-yellow"> 0%</span>
            </a>
          </li>
          @else

          <li>
            <a href="#">DUT
              <span class="pull-right text-red"> {{ ($total_dut/$total_members)*100 }}%</span>
            </a>
          </li>
          <li>
            <a href="#">DEUP
              <span class="pull-right text-green"> {{ ($total_deup/$total_members)*100 }}%</span>
            </a>
          </li>
          <li>
            <a href="#">AUTRE
              <span class="pull-right text-yellow"> {{ ($total_autre/$total_members)*100 }}%</span>
            </a>
          </li>

          @endif
        </ul>
      </div>
      <!-- /.footer -->
    </div>
    <!-- /.box -->
  </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<!-- DataTables -->
<script src="{{ asset('components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('components/chart.js/Chart.js') }}"></script>
<script>
  $(function () {
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    });



      // -------------
  // - PIE CHART -
  // -------------
  // Get context with jQuery - using jQuery's .get() method.
  var pieChartCanvas = $('#pieChart').get(0).getContext('2d');
  var pieChart       = new Chart(pieChartCanvas);
  var PieData        = [
    {
      value    : {{ $total_dut }},
      color    : '#f56954',
      highlight: '#f56954',
      label    : 'DUT'
    },
    {
      value    : {{ $total_deup }},
      color    : '#00a65a',
      highlight: '#00a65a',
      label    : 'DEUP'
    },
    {
      value    : {{ $total_autre }},
      color    : '#f39c12',
      highlight: '#f39c12',
      label    : 'AUTRE'
    }
  ];
  var pieOptions     = {
    // Boolean - Whether we should show a stroke on each segment
    segmentShowStroke    : true,
    // String - The colour of each segment stroke
    segmentStrokeColor   : '#fff',
    // Number - The width of each segment stroke
    segmentStrokeWidth   : 1,
    // Number - The percentage of the chart that we cut out of the middle
    percentageInnerCutout: 50, // This is 0 for Pie charts
    // Number - Amount of animation steps
    animationSteps       : 100,
    // String - Animation easing effect
    animationEasing      : 'easeOutBounce',
    // Boolean - Whether we animate the rotation of the Doughnut
    animateRotate        : true,
    // Boolean - Whether we animate scaling the Doughnut from the centre
    animateScale         : false,
    // Boolean - whether to make the chart responsive to window resizing
    responsive           : true,
    // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
    maintainAspectRatio  : false,
    // String - A legend template
    legendTemplate       : '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<segments.length; i++){%><li><span style=\'background-color:<%=segments[i].fillColor%>\'></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>',
    // String - A tooltip template
    tooltipTemplate      : '<%=value %> <%=label%>'
  };
  // Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  pieChart.Doughnut(PieData, pieOptions);
  // -----------------
  // - END PIE CHART -
  // -----------------

  });
</script>
@endpush