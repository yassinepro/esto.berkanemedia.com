<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>la liste des candidats</title>
    <link href="{{ asset('js/dt/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('js/dt/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('js/dt/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
    <style>
        body {
            padding-top:100px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <table id="example" class="table table-striped table-bordered table-hover recu-dataTables">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nom</th>
                                <th>Prénom</th>
                                <th>Date de naissance</th>
                                <th>CNE</th>
                                <th>Série du Bac</th>
                                <th>Année de Bac</th>
                                <th>Moyenne du Bac</th>
                                <th>Type de diplôme Bac+2</th>
                                <th>Spécialité du diplôme Bac+2</th>

                                <th>Année d'inscription</th>
                                <th>Année d'obtention</th>
                                <th>Moyenne de la 1<sup>ère</sup> année</th>
                                <th>Moyenne de la 2<sup>éme</sup> année</th>
                                <th>le nombre d'année sans activité</th>
                                <th>Total des Prérequis</th>
                                <th>Email</th>
                                <th>Téléphone</th>
                                <th>CIN</th>
                                <th>Ville</th>
                                <th>Adresse</th>
                                <th>Zip Code</th>
                                <th>Genre</th> <th>Photo</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($users_diplome->unique('user_id') as $user)
                            <tr>
                                <td>{{ $user->user_id}} </td>
                                <td>{{ title_case($user->family_name) }}</td>
                                <td>{{ title_case($user->first_name) }}</td>
                                <td>{{ $user->birth_date }}</td>
                               @foreach($users_bac->unique('user_id') as $user_bac)
                                    @if($user->user_id == $user_bac->user_id)
                                    <td>{{ $user_bac->cne }}</td>
                                    <td>{{ $user_bac->entitled }}</td>
                                    <td>{{ $user_bac->year_of_graduation }}
                                    <td>{{ $user_bac->average }}</td>
</td>
                                    @endif
                                @endforeach
                                <td>{{ $user->type }} </td>
                                <td>{{ $user->entitled }} </td>

                                <td>{{ $user->year_of_registration }} </td>
                                <td>{{ $user->year_of_graduation }} </td>
                                <td>{{ $user->first_average }} </td>
                                <td>{{ $user->second_average }} </td>
                                <td>{{ $user->number_year_stop }} </td>
                                <td>
                                    <script>
                                        
                                        document.write(("{{ $user->prerequisites_list }}".match(/,/g) || []).length + 1);
                                    </script>
                                </td>
                                <td>{{ $user->email }}</td>

                                <td>{{ $user->phone }}</td>
                                 <td>{{ $user->id_number }}</td>
                                <td>{{ $user->city }}</td>
                                <td>{{ $user->address }}</td>
                                <td>{{ $user->zip_code }}</td>
                                <td>{{ $user->gender }}</td>
                                <td><img src="uploads/images/{{ $user->image }}" width='100px' height='100px'></td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('js/dt/js/jquery-1.12.4.js') }}"></script>
<script src="{{ asset('js/dt/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('js/dt/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('js/dt/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('js/dt/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('js/dt/js/jszip.min.js') }}"></script>
<script src="{{ asset('js/dt/js/pdfmake.min.js') }}"></script>
<script src="{{ asset('js/dt/js/vfs_fonts.js') }}"></script>
<script src="{{ asset('js/dt/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('js/dt/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('js/dt/js/buttons.colVis.min.js') }}"></script>


<script>
    $(document).ready(function() {

        $(document).ready(function() {
            var table = $('#example').DataTable( {
                lengthChange: false,
                buttons: [ 'copy', 'excel', 'pdf','print' ],
                "columnDefs": [
                    { "targets": [4,5,6,7,8,9,10,11], "searchable": false }
                ]
            } );

            table.buttons().container().appendTo( '#example_wrapper .col-sm-6:eq(0)' );
        } );
    });
</script> 
</body>
</html>