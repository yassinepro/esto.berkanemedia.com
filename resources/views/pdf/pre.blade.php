<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Pré-inscription en ligne</title>
	<link rel="stylesheet" href="<?php echo public_path();?>/css/font-awesome.css">
	<link rel="stylesheet" href="<?php echo public_path();?>/css/bootstrap.min.css">
	<style>
		body {
			margin: 0px;
			padding: 0px;
		}
		.logo {
			height: 100px;
		}
		.h1 {
			font-size: 14px;
		}
		.h2 {
			font-size: 13px;
		}
		.h3 {
			font-size: 11px;
		}
		.h4 {
			font-size: 10px;
		}
		.h5 {
			font-size: 12px;
		}
		h3 {
			font-size: 14px;
			font-weight: bold;	
		}
		.rm {
			height :40px;
		}
		.picture {
			width:3cm;
			height:3cm;
			float:left;
			margin-right:10px;
		}
		label {
			font-weight: normal !important;
		}
		.admin legend {
			font-size : 14px;
			border:none;
			padding:10px;
		}
		.admin fieldset {
			border: 1px dashed black !important;
			/* height : 4cm; */
			padding : 15px 20px;
		}
		.admin fieldset p {
			border-bottom: 1px dotted #000 !important;
			padding-top : 0.5cm;
		}
		.checked {
			width:16px;
		}
		hr {
			margin-top: 10px;
    		margin-bottom: 10px;
		}
	</style>
</head>
<body>
	<table width="100%" style="margin-top:-50px;">
		<tr>
			<td align="left" valign="bottom">
				<img class="logo" src="<?php echo public_path();?>/images/header.jpg">
			</td>
		</tr>
	</table>
	<div class="container">
		<div class="row">

			<div class="col-xs-12 text-center">
				<hr style="height:2px; color:black;">
				<h3>Fiche de Pré-inscription en ligne</h3>
				Licence Professionnelle {{ $config->filiere }} ({{ $config->abbreviation }})
				<hr>
			</div>
		</div>

		<div class="informtion_personelles">
			<div class="row">
				<div class="col-xs-12">
					<h3>Informations Personnelles</h3>
					<img class="picture" src="<?php echo public_path();?>/uploads/images/{{ $information->image }}">
					Numéro d'inscription : {{ $config->abbreviation }}-{{ $year }} | {{ $information->user_id}} <br>
					Nom : {{ $information->family_name}} <br>
					Prénom : {{ $information->first_name}} <br>
					CNE : {{ $cne }} <br>
					CIN : {{ $information->id_number}}
				</div>
			</div>
		</div>

		<div class="diplome">
			<div class="row">
				<div class="col-xs-12">
					<hr>
					<h3>Diplôme</h3>
					Type de diplôme : {{ $diploma->type}} <br>
					Intitulé du diplôme : {{ $diploma->entitled }} <br>
					Année d'inscription : {{ $diploma->year_of_registration }} <br>
					Année d'obtention : {{ $diploma->year_of_graduation }} <br>
					Moyenne de la 1<sup>ère</sup> année : {{ $diploma->first_average }} <br>
					Moyenne de la 2<sup>éme</sup> année : {{ $diploma->second_average }}
				</div>
			</div>
		</div>

		<div class="prerequis">
			<div class="row">
				<div class="col-xs-12">
					<hr>
					<h3>Prérequis ({{ count($prerequisite) }}/{{ count($prerequis) }})</h3>
					@foreach ($prerequis as $item)
						@if(in_array($item , $prerequisite)) 
							<img class="checked" src="<?php echo public_path();?>/images/checked.png"> {{ $item }}
						@else
							<img class="checked" src="<?php echo public_path();?>/images/unchecked.png"> {{ $item }}
						@endif
						<br>
					@endforeach
				</div>
			</div>
		</div>

		<div class="admin">
			<div class="row">
				<div class="col-xs-12">
					<hr>
					<table width="100%" style="margin-bottom:1cm;">
						<tr>
							<td valign="top" width="50%">
								<h3>Décision de la commission</h3><br>
								<img class="checked" src="<?php echo public_path();?>/images/unchecked.png"> Dossier Accepté <br>
								<img class="checked" src="<?php echo public_path();?>/images/unchecked.png"> Dossier Rejeté <br><br>
							</td>
							<td valign="top" width="50%" style="border-left:1px solid black; padding-left:10px;">
								<h3>Signature</h3>
								<p>J'ai bien relu mon dossier et je certifie l'exactitude des informations qu'il contient</p>
								<br><br><br>
							</td>
						</tr>
					</table>
					<fieldset>
						<legend> Motif : </legend>
						<p></p>
						<p></p>
						<p></p>
					</fieldset>
				</div>
			</div>
		</div>

	</div>
</body>
</html>