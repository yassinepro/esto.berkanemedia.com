<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
	<style>
		body {
			width: 21cm;
			min-height: 29.7cm;
			padding: 1cm;
			margin: 1cm auto;
			border: 1px #D3D3D3 solid;
			background: white;
		}
		.logo {
			height: 80px;
		}
		.h1 {
			font-size: 14px;
		}
		.h2 {
			font-size: 13px;
		}
		.h3 {
			font-size: 11px;
		}
		.h4 {
			font-size: 10px;
		}
		.h5 {
			font-size : 12px;
		}
		h3 {
			font-size : 14px;
			font-weight: bold;	
		}
		.rm {
			height : 40px;
		}
		.picture {
			width:3cm;
			height:3cm;
			float:left;
			margin-right:10px;
		}
		label {
			font-weight: normal !important;
		}
		.admin legend {
			font-size : 14px;
			padding :10px;
			border:none;
		}
		.admin fieldset {
			border: 1px dashed black !important;
			height : 200px;
		}
	</style>
</head>
<body>
	<table width="100%">
		<tr>
			<td align="left" valign="bottom">
				<img class="logo" src="{{ asset('images/ump.png') }}">
			</td>
			<td valign="middle" style="text-align:center; line-height:16px;">
				<img class="rm" src="{{ asset('images/Royaume-du-Maroc.png') }}"> <br>
				<span class="h1">Royaume du Maroc</span> <br>
				<span class="h1">Université Mohammed Premier Oujda</span> <br>
				<span class="h2">Ecole Supérieur de Technologie Oujda</span> <br>
				<span class="h3">Département Génie Informatique</span>
				<!-- <span class="h4">Licence Professionnelle</span> -->
				<!-- <span class="h5">Informatique Décisionnelle (LPID)</span> -->
			</td>
			<td align="right" valign="bottom">
				<img class="logo" src="{{ asset('images/logoesto.png') }}">
			</td>
		</tr>
	</table>
	<div class="container">
		<div class="row">

			<div class="col-xs-12 text-center">
				<hr>
				fiche de pré-inscription en ligne <br>
				Licence Professionnelle d'Informatique Décisionnelle (LPID)
				<hr>
			</div>
		</div>

		<div class="informtion_personelles">
			<div class="row">
				<div class="col-xs-12">
					<h3>Informations Personnelles</h3>
					<img class="picture" src="{{ asset('uploads/images') }}/{{ $information->image }}">
					Numéro d'inscription : LPID-{{ $year }}{{ $information->user_id}} <br>
					Nom : {{ $information->family_name}} <br>
					Prénom : {{ $information->first_name}} <br>
					CNE : {{ $cne }} <br>
					CIN : {{ $information->id_number}}
				</div>
			</div>
		</div>

		<div class="diplome">
			<div class="row">
				<hr>
				<div class="col-xs-12">
					<h3>Diplôme</h3>
					Type de diplôme : {{ $diploma->type}} <br>
					Intitulé du diplôme : {{ $diploma->entitled }} <br>
					Année d'inscription : {{ $diploma->year_of_graduation }} <br>
					Année d'obtention : {{ $diploma->year_of_graduation }} <br>
					Moyenne de la 1<sup>ère</sup> année : {{ $diploma->first_average }} <br>
					Moyenne de la 2<sup>éme</sup> année : {{ $diploma->second_average }}
				</div>
			</div>
		</div>
		<div class="prerequis">
			<div class="row">
				<hr>
				<div class="col-xs-12">
					<h3>Prérequis ({{ count($prerequisite) }}/13)</h3>
					<table class="condi col-md-6">
						<tbody>
							<tr>
								<td width="250px">
									<input @if(in_array('Algorithme', $prerequisite)) checked @endif id="algo" type="checkbox" value="Algorithme" name="prerequisite_list[]"> <label for="algo">Algorithme </label>
								</td>
								<td>
									<input @if(in_array('c++', $prerequisite)) checked @endif id="cplusplus" type="checkbox" name="prerequisite_list[]" value="c++"> <label for="cplusplus">C++ </label>
								</td>
							</tr>
							<tr>
								<td>
									<input @if(in_array('Java', $prerequisite)) checked @endif id="java" type="checkbox" value="Java" name="prerequisite_list[]"> <label for="java">Java </label>
								</td>
								<td>
									<input @if(in_array('C', $prerequisite)) checked @endif id="c" type="checkbox" value="C" name="prerequisite_list[]"> <label for="c">C </label>
								</td>
							</tr>
							<tr>
								<td>
									<input @if(in_array('Architecture des ordinateurs', $prerequisite)) checked @endif id="arch" type="checkbox" value="Architecture des ordinateurs" name="prerequisite_list[]"> <label for="arch"> Architecture des ordinateurs </label>
								</td>
								<td>
									<input @if(in_array('Systèmes d\'exploitation', $prerequisite)) checked @endif id="systeme" type="checkbox" value="Systèmes d'exploitation" name="prerequisite_list[]"> <label for="systeme"> Systèmes d'exploitation </label>
								</td>
							</tr>
							<tr>
								<td>
									<input @if(in_array('Réseaux informatique', $prerequisite)) checked @endif id="reseauxinfo" type="checkbox" value="Réseaux informatique" name="prerequisite_list[]"> <label for="reseauxinfo"> Réseaux informatique </label>
								</td>
								<td>
									<input @if(in_array('Recherche opérationnelle', $prerequisite)) checked @endif id="rechercheop" type="checkbox" value="Recherche opérationnelle" name="prerequisite_list[]"> <label for="rechercheop"> Recherche opérationnelle </label>
								</td>
							</tr>
							<tr>
								<td>
									<input @if(in_array('Statistique', $prerequisite)) checked @endif id="stat" type="checkbox" value="Statistique" name="prerequisite_list[]"> <label for="stat"> Statistique </label>
								</td>
								<td>
									<input @if(in_array('Porbabilité', $prerequisite)) checked @endif id="proba" type="checkbox" name="prerequisite_list[]" value="Porbabilité"> <label for="proba"> Porbabilité </label>
								</td>
							</tr>
							<tr>
								<td>
									<input @if(in_array('Algèbre', $prerequisite)) checked @endif id="algebre" type="checkbox" value="Algèbre" name="prerequisite_list[]"> <label for="algebre"> Algèbre </label>
								</td>
								<td>
									<input @if(in_array('Analyse', $prerequisite)) checked @endif id="analyse" type="checkbox" name="prerequisite_list[]" value="Analyse"> <label for="analyse"> Analyse </label>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<input @if(in_array('Français & Anglais', $prerequisite)) checked @endif id="fren" type="checkbox" value="Français & Anglais" name="prerequisite_list[]"> <label for="fren"> Français &amp; Anglais </label>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
		<div class="admin">
			<div class="row">
				<hr>
				<fieldset>
					<legend>zone d'administration</legend>
					<p>
					</p>
				</fieldset>
			</div>
		</div>

	</div>
</body>
</html>