@extends('layouts.dashboard')

@section('title', 'ESTO | Ecole Supérieure de Technologie - Oujda')

@section('breadcrumb', 'Mon dossier')

@push('styles')
  <style>
        label {
			font-weight: normal !important;
		}
		.admin legend {
			font-size : 14px;
			border:none;
		}
		.admin fieldset {
			border: 1px dashed black !important;
			height : 4cm;
			padding : 20px;
		}
		.checked {
			width:16px;
		}
        .picture {
			width:3cm;
			height:3cm;
			float:left;
			margin-right:10px;
		}
  </style>
@endpush
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">Mon dossier</h3>
        </div>
        <!-- /.box-header -->
            <div class="box-body">
                <div class="col-xs-12">
                    <form action="{{ route('dossiervalide') }}" method="POST">
                        @csrf
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-check"></i> Dossier reçu et complet
                        </button>
                        <a href="{{ route('home') }}" class="btn btn-default">liste des candidats</a>
                        <input type="hidden" value="{{ $information->user_id }}" name="code">
                    </form>
                </div>
                <div class="col-xs-12">
                    <div class="informtion_personelles">
                        <h3>Informations Personnelles</h3>
                        <img class="picture" src="{{ asset('uploads/images/'.$information->image) }}">
                        Numéro d'inscription : {{ $config->abbreviation }}-{{ $year }} | {{ $information->user_id}} <br>
                        Nom : {{ $information->family_name}} <br>
                        Prénom : {{ $information->first_name}} <br>
                        CNE : {{ $cne }} <br>
                        CIN : {{ $information->id_number}}
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="diplome">
                        <hr>                        
                        <h3>Diplôme</h3>
                        Type de diplôme : {{ $diploma->type}} <br>
                        Intitulé du diplôme : {{ $diploma->entitled }} <br>
                        Année d'inscription : {{ $diploma->year_of_registration }} <br>
                        Année d'obtention : {{ $diploma->year_of_graduation }} <br>
                        Moyenne de la 1<sup>ère</sup> année : {{ $diploma->first_average }} <br>
                        Moyenne de la 2<sup>éme</sup> année : {{ $diploma->second_average }}
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="prerequis">
                        <hr>
                        <h3>Prérequis ({{ count($prerequisite) }}/{{ count($prerequis) }})</h3>
                        <ul type="none">
                            @foreach ($prerequis as $item)
                                @if(in_array($item , $prerequisite)) 
                                    <li>
                                        <img class="checked" src="{{ asset('images/checked.png')}}"> {{ $item }}
                                    </li>
                                @else
                                    <li>
                                        <img class="checked" src="{{ asset('images/unchecked.png')}}"> {{ $item }}
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
  <!-- /.col -->
</div>
@endsection

@push('scripts')

@endpush