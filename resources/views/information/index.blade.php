@extends('layouts.apply',['personalInformation'=>$personalInformation])

@section('title', 'ESTO | Mes Informations Personnelles #1')

@section('breadcrumb', 'Mes Informations Personnelles')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/intl-tel-input/css/intlTelInput.css') }}">
@endpush

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Mon dossier</h3>
      </div>
      <!-- /.box-header -->
      <form role="form" action="{{ route('information.update',['id' => $personalInformation->id]) }}" method="post" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="box-body">
          <div class="box-group" id="accordion">
            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
            <div class="panel box box-primary">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                    Mes Informations Personnelles #1
                  </a>
                </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in">
                <div class="box-body">
                  <fieldset class="col-md-12">
                    <legend>Identité</legend>
                    <div class="col-md-6">
                      <div class="form-group {{ $errors->has('image') ? 'has-error' : ''}}">
                        <label for="image">Photo</label>
                        <img id="image" class="profile-user-img img-responsive img-circle" src="{{ route('welcome') }}/uploads/images/{{ $personalInformation->image }}" alt="User profile picture">
                        <input onchange="readURL(this);" name="image" value="{{ route('welcome') }}/uploads/images/{{ $personalInformation->image }}" type="file" id="image" accept="image/png,jpg">

                        <p class="help-block">Le fichier doit être au format JPG ou PNG et ne doit pas dépasser une taille de 1024 ko.</p>
                      </div>
                      <div class="form-group {{ $errors->has('family_name') ? 'has-error' : ''}}">
                        <label for="family_name">Nom de famille * :</label>
                        <input value="{{ $personalInformation->family_name }}" id="family_name" name="family_name" type="text" class="form-control">
                      </div>
                      <div class="form-group {{ $errors->has('first_name') ? 'has-error' : ''}}">
                        <label for="first_name">Prénom * :</label>
                        <input value="{{ $personalInformation->first_name }}" id="first_name" name="first_name" type="text" class="form-control">
                      </div>
                      <div class="form-group {{ $errors->has('gender') ? 'has-error' : ''}}">
                        <label for="">Sexe * :</label>
                        <div class="checkbox">
                          <label style="margin-right:15px;">
                            <input type="radio" value="M" name="gender" {{ $personalInformation->gender =="M" ? 'checked' : '' }}> Masculin
                          </label>
                          <label>
                            <input type="radio" value="F" name="gender" {{ $personalInformation->gender =="F" ? 'checked' : '' }}> Féminin
                          </label>
                        </div>
                      </div>
                    </div> <!-- ./col-md-6  -->

                    <div class="col-md-6">
                      <!-- Date dd/mm/yyyy -->
                      <div class="form-group {{ $errors->has('birth_date') ? 'has-error' : ''}}">
                        <label>Date de naissance * :</label>

                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input name="birth_date" value="{{ $personalInformation->birth_date }}" id="datemask" type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                        </div>
                      </div>
                      <div class="form-group {{ $errors->has('native_country') ? 'has-error' : ''}}">
                        <label>Pays de naissance * :</label>
                        <select name="native_country" class="form-control">
                            <option value=""></option>
                            @foreach($pays as $pay)
                              <option value="{{ $pay->id }}" {{ $personalInformation->native_country == $pay->id ? 'selected' : '' }}>{{ $pay->nom_fr_fr}}</option>
                            @endforeach
                        </select>
                      </div>
                      <div class="form-group {{ $errors->has('place_of_birth') ? 'has-error' : ''}}">
                        <label>Lieu de naissance * :</label>
                        <input name="place_of_birth" type="text" class="form-control" name="place_of_birth" value="{{ $personalInformation->place_of_birth }}">
                      </div>
                      <div class="form-group {{ $errors->has('type_id') ? 'has-error' : ''}}">
                        <label>Type de pièce d'identité * :</label>
                        <select name="type_id" class="form-control">
                          <option value=""> </option>
                          <option value="1" {{ $personalInformation->type_id == "1" ? 'selected' : '' }}>Carte d'identité nationale</option>
                          <option value="2" {{ $personalInformation->type_id == "2" ? 'selected' : '' }}>Passeport</option>
                        </select>
                      </div>
                      <div class="form-group {{ $errors->has('id_number') ? 'has-error' : ''}}">
                        <label>Numéro de pièce d'identité * :</label>
                        <input value="{{ $personalInformation->id_number }}" name="id_number" type="text" class="form-control">
                        @if($errors->has('id_number'))
                          <span class="help-block">
                            {{ $errors->first('id_number') }}
                          </span>
                        @endif
                      </div>
                    </div> <!-- ./col-md-6 -->
                  </fieldset>
                  <fieldset class="col-md-12">
                    <legend>Coordonnées</legend>
                    <div class="col-md-6">
                      <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                        <label>Adresse * :</label>
                        <textarea class="form-control" name="address" rows="5">{{ $contactInformation->address }}</textarea>
                      </div>
                      <div class="form-group {{ $errors->has('zip_code') ? 'has-error' : ''}}">
                        <label for="">Code postal * :</label>
                        <input value="{{ $contactInformation->zip_code }}" name="zip_code" type="text" class="form-control">
                      </div>
                      <div class="form-group {{ $errors->has('region') ? 'has-error' : ''}}">
                        <label for="">Province / état / région * :</label>
                        <select name="region" id="region" class="form-control">
                            <option value=""></option>
                            @foreach ($regions as $region)
                              <option value="{{ $region->id }}" {{ $contactInformation->region == $region->id ? 'selected' : '' }}>{{ $region->region }}</option>
                            @endforeach
                        </select>
                      </div>
                    </div> <!-- ./col-md-6 -->
                    <div class="col-md-6">
                      <div class="form-group {{ $errors->has('city') ? 'has-error' : ''}}">
                        <label for="">Ville * :</label>
                        <select name="city" id="city" class="form-control">
                            <option value="{{ $contactInformation->city }}">{{ $contactInformation->city }}</option>
                        </select>
                        <p class="help-block">Si elle n'est pas présente dans la liste, je sélectionne la mention Autre et je saisis son nom ci-dessous :</p>
                        <input name="auther_city" type="text" class="form-control">
                      </div>
                      <div class="form-group {{ $errors->has('phone') ? 'has-error' : ''}}">
                        <label for="">N° de téléphone :</label>
                        <input value="{{ $contactInformation->phone }}" name="phone" type="tel" id="phone" class="form-control">
                      </div>
                      <div class="form-group">
                        <button type="submit" class="btn btn-primary">Update</button>
                      </div>
                    </div> <!-- ./col-md-6 -->
                  </fieldset>
                </div>
              </div>
            </div>
            <div class="panel box box-danger">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a href="{{ route('diploma.index') }}">
                    Mon parcours et mes diplômes #2
                  </a>
                </h4>
              </div>
            </div>
            <div class="panel box box-success">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a href="{{ route('home') }}">
                    Prérequis & Validation #3
                  </a>
                </h4>
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
      </form>
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
  <div class="col-md-12">
    
  </div>
</div>
@endsection

@push('scripts')
<!-- InputMask -->
<script src="{{ asset('js/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('js/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('js/input-mask/jquery.inputmask.extensions.js') }}"></script>

<script src="{{ asset('js/intl-tel-input/js/intlTelInput.js') }}"></script>
<script>
  $("#phone").intlTelInput();
</script>
<script>
  $(function() {
    //Datemask dd/mm/yyyy
    $('#datemask, #datemask1').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
  });
</script>
<script>
    $(function(){

        // $('#cursus').change(function(){
        //     var content = '#modal-' + $(this).val();
        //     $('#cursus-btn').attr('data-target',content);
        // });


        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

        // $('#add-bac').click(function(){
        //     $.post("{{ route('diploma.store')}}",
        //     {
        //         year_of_graduation: $('#year_of_graduation').val(),
        //         country: $('#country').val(),
        //         region: $('#region').val(),
        //         city: $('#city').val(),
        //         cne: $('#cne').val(),
        //         average: $('#average').val(),
        //         first_average: null,
        //         second_average: null,
        //         type: 'Baccalauréat'
        //     },
        //     function(data){
        //         data = eval('(' + data + ')');
        //         $('#cursus > strong').attr('style','display:none;');
        //         $('#cursus > table').attr('style','display:table;');
        //         $('#cursus > table > tbody').append(`
        //                 <tr>
        //                   <td>${ data.id }.</td>
        //                   <td>${ data.year_of_graduation }</td>
        //                   <td>${ data.type }</td>
        //                   <td>
        //                     <button type="button" class="btn btn-danger btn-xs">Supprimer</button>
        //                   </td>
        //                 </tr>
        //             `);                
        //     });
        // });

        // $('#region').change(function(){

        //     $.post("{{ route('villes')}}",
        //     {
        //         region: $('#region').val()
        //     },
        //     function(data){
        //         data = eval('(' + data + ')');
        //         $('#city').html('<option value=""></option>');
        //         $.each( data, function( key, item ) {
        //           $('#city').append('<option value="'+item.id+'">'+ item.ville +'</option>');
        //         });
        //         $('#city').append('<option value="Autre">Autre</option>');
        //     });
        // });

        $('#region').change(function(){

            $.post("{{ route('villes')}}",
            {
                region: $('#region').val()
            },
            function(data){
                data = eval('(' + data + ')');
                $('#city').html('<option value=""></option>');
                $.each( data, function( key, item ) {
                  $('#city').append('<option value="'+item.ville+'">'+ item.ville +'</option>');
                });
                $('#city').append('<option value="Autre">Autre</option>');
            });
        });

        // function valider(){
        //     alert();
        // }

        // $('#certify').change(function(){
        //     if($('#certify').is(':checked') == true){
        //         $('#certify-btn').removeClass('disabled');
        //         $('#certify-btn').attr('type','submit');
        //         $('#certify-btn').attr('onclick','return true;');
        //     }else{
        //         $('#certify-btn').addClass('disabled');
        //         $('#certify-btn').attr('onclick','return false;');
        //     }
        // });

        // $('#diplome-region').change(function(){

        //     $.post("{{ route('villes')}}",
        //     {
        //         region: $('#diplome-region').val()
        //     },
        //     function(data){
        //         data = eval('(' + data + ')');
        //         $('#diplome-city').html('<option value=""></option>');
        //         $.each( data, function( key, item ) {
        //           $('#diplome-city').append('<option value="'+item.id+'">'+ item.ville +'</option>');
        //         });
        //         $('#diplome-city').append('<option value="Autre">Autre</option>');
        //     });
        // });

    });

      function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#image')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
@endpush