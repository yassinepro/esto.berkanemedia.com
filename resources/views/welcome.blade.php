<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="HandheldFriendly" content="True">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<meta name="description" content="" />
<meta name="keywords" content="">
<meta name="author" content="">
<title>ESTO - Ecole Supérieure de Technologie - Oujda.</title>

<!--Import Favicon-->
<link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">

<!--Import Google Font-->
<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300|Kanit:400,600|Open+Sans" rel="stylesheet">

<!--Import ION Icon Fonts-->
<link rel="stylesheet" href="{{ asset('css/ionicons.css') }}">

<!--Import Normalize CSS-->
<link rel="stylesheet" href="{{ asset('css/normalize.css') }}">

<!--Import Superslides CSS-->
<link rel="stylesheet" href="{{ asset('css/superslides.css') }}">

<!--Import Style CSS-->
<link rel="stylesheet" href="{{ asset('css/style.css') }}">

<!--Import Modernizr JS-->
<script src="{{ asset('js/modernizr-custom.js') }}"></script>
</head>

<body>

<!-- Preloader -->
<div class="preloader"></div>
<!-- End Preloader -->

<!-- Logo -->
<div class="logo">
    <a class="text-center" href="{{ route('welcome') }}" title="ESTO">
        <img width="150px" src="images/logo.svg" alt="EST Oujda"><br>
        <img width="200px" src="images/title.svg" alt="EST Oujda">
    </a>
</div>
<!-- End Logo -->

<!-- Sidebar -->
<div class="sidebar">

    <!-- Sidebar Toggle Button --> 
    <div class="toggle-sidebar"></div>
    <!-- End Sidebar Toggle Button -->

    <!-- Content -->
    <div class="content">

        <!-- Coming Soon Message -->
        <p>
            <img width="200px" src="images/UMPESTO.png" alt="EST Oujda">
        </p>
        <h1 class="site-title"><span>Licence <span>P</span>rofessionnelle</span></h1>
        <p class="message">{{ $config->filiere }}</p>
        <!-- <a href="{{ route('login') }}">Login</a>
        <a href="{{ route('register') }}">Register</a> -->
        <!-- End Coming Soon Message -->

        <!-- Countdown -->
        <script>
        TargetDate = "01/01/2018 10:00 AM";
        FinishMessage = "<div><a class=\"login-button\" href=\"{{ route('login') }}\">Connexion</a> <a class=\"register-button\" href=\"{{ route('register') }}\">Je m'inscris</a></div>";
        CountActive = true;
        BackColor = "transparent";
        ForeColor = "inherit";
        CountStepper = -1;
        LeadingZero = false;
        DisplayFormat = "<h2>%%D%% Days Left</h2>";
        </script>
        <script src="{{ asset('js/countdown.js') }}"></script>
        <!-- End Countdown -->

        <!-- Buttons -->
        <!-- <a href="javascript:void(0);" class="newsletter-button" title="Subscribe to our Newsletter"><i class="ion-ios-bell-outline"></i>s'abonner</a> -->
        <a href="{{ $config->avis }}" target="_blank" class="newsletter-button" title="Avis"><i class="ion-ios-bell-outline"></i>Avis</a>
        <a href="javascript:void(0);" class="contact-button" title="Send an email"><i class="ion-ios-location-outline"></i> Contact</a>
        <!-- End Buttons -->

        <!-- Social Networks 
        <ul class="socialnetwork">
            <li><a href="https://facebook.com" target="_blank" class="ion-social-facebook"></a></li>
            <li><a href="https://twitter.com" target="_blank" class="ion-social-twitter"></a></li>
            <li><a href="https://instagram.com" target="_blank" class="ion-social-instagram-outline"></a></li>
            <li><a href="https://linkedin.com" target="_blank" class="ion-social-linkedin"></a></li>
        </ul>-->
        <!-- End Social Networks 

        <div class="links">
            <a href="http://www.ump.ma/fr/esto/informatique-decisionnelle" target="_blank"><i class="ion-clipboard"></i> Conditions d'accès</a>
            <a href="http://www.ump.ma/fr/esto/informatique-decisionnelle" target="_blank"><i class="ion-clipboard"></i> Objectif de la formation</a>
        </div>-->
        
    </div>
    <!-- End Content -->

</div>
<!-- End Sidebar -->

<!-- Newsletter -->
<div id="newsletter" class="page">

    <!-- Page Content --> 
    <div class="page-content">

        <!-- Page Title -->
        <!-- Subscribe to our newsletter -->
        <h2>Recevez une notification <i class="ion-paper-airplane"></i></h2>
        <!-- End Page Title -->

        <!-- Newsletter Form -->
        <form id="newsletter-form" action="#" method="post">
            @csrf
            <input type="email" name="nemail" placeholder="Entrer votre Email" required>
            <button type="submit" name="action">Soumettre</button>
        </form>
        <!-- End Newsletter Form -->

        <!-- Newsletter Success Message -->
        <div class="newsletter-form-success">
            <p>Thank you for subscribing to our newsletter.</p>
        </div>
        <!-- End Newsletter Success Message -->

        <!-- Close Button -->
        <a href="javascript:void(0);" class="close ion-ios-close"></a>
        <!-- End Close Button -->

    </div>
    <!-- End Page Content --> 
    
</div>
<!-- End Newsletter -->

<!-- Contact -->
<div id="contact" class="page">
    
    <!-- Page Content --> 
    <div class="page-content">

        <!-- Page Title -->
        <h2>Prenez contact avec nous <i class="ion-ios-location-outline"></i></h2>
        <!-- End Page Title -->

        <!-- Contact Address -->
        <div class="contact-address">
            <p><i class="ion-ios-book-outline"></i>BP 473 Complexe universitaire Al Qods Oujda 60000</p>
            <p><i class="ion-ios-telephone-outline"></i>{{ $config->tel }}</p>
            <p><i class="ion-ios-printer-outline"></i>{{ $config->fix }} / {{ $config->fax }}</p>
            <p><i class="ion-ios-email-outline"></i> <a href="mailto:{{ $config->email }}">{{ $config->email }}</a></p>
            <iframe width="100%" height="200px" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=EST%20oujda&key=AIzaSyA5wlyEN7B9Q-nFxVf_MWUc1BWv1sIJqWc" allowfullscreen></iframe>
        </div>
        <!-- End Contact Address -->

        <!-- Close Button -->
        <a href="javascript:void(0);" class="close ion-ios-close"></a>
        <!-- End Close Button -->

    </div>
    <!-- End Page Content --> 

</div>
<!-- End Contact -->

<!-- Slides -->
<div id="slides">
  <div class="slides-container">
    <img src="{{ asset('images/slide01.jpg') }}" alt=""/>
  </div>
</div>
<!-- End Slides -->

<!--Import jQuery JS-->
<script src="{{ asset('js/jquery.min.js') }}"></script>

<!--Import jQuery Migrate JS-->
<script src="{{ asset('js/jquery-migrate.js') }}"></script>

<!--Import jQuery Form JS-->
<script src="{{ asset('js/jquery.form.js') }}"></script>

<!--Import jQuery Superslides JS-->
<script src="{{ asset('js/jquery.superslides.js') }}"></script>

<!--Import Index JS-->
<script src="{{ asset('js/index.js') }}"></script>

</body>
</html>
