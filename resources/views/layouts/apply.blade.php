<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
    <link rel="stylesheet" href="{{ asset('css/skins/skin-blue.min.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    @stack('styles')        
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="/" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>E</b>STO</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>EST</b>Oujda</span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              @if(Auth::user()->avatar =='default_avatar.png')
                <img src="{{ route('welcome') }}/images/{{ Auth::user()->avatar }}" class="user-image" alt="{{ title_case(Auth::user()->name) }}">
              @else
                <img src="{{ route('welcome') }}/uploads/images/{{ Auth::user()->avatar }}" class="user-image" alt="{{ title_case(Auth::user()->name) }}">
              @endif
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs">{{ title_case(Auth::user()->name) }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
                @if(Auth::user()->avatar =='default_avatar.png')
                  <img src="{{ route('welcome') }}/images/{{ Auth::user()->avatar }}" class="img-circle" alt="{{ title_case(Auth::user()->name) }}">
                @else
                  <img src="{{ route('welcome') }}/uploads/images/{{ Auth::user()->avatar }}" class="img-circle" alt="{{ title_case(Auth::user()->name) }}">
                @endif

                <p>
                  {{ title_case(Auth::user()->name) }}
                </p>
              </li>
              <!-- Menu Body -->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="javascript:void(0);" class="btn btn-default btn-flat disabled">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="btn btn-default btn-flat">Sign out</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>                                                     
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          @if(Auth::user()->avatar =='default_avatar.png')
            <img src="{{ route('welcome') }}/images/{{ Auth::user()->avatar }}" class="img-circle" alt="{{ title_case(Auth::user()->name) }}">
          @else
            <img src="{{ route('welcome') }}/uploads/images/{{ Auth::user()->avatar }}" class="img-circle" alt="{{ title_case(Auth::user()->name) }}">
          @endif
        </div>
        <div class="pull-left info">
          <p>{{ title_case(Auth::user()->name) }}</p>
          <!-- Status -->
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Licence Professionnelle</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="{{ request()->is('home') ? 'active' : '' }}"><a href="{{ route('home') }}"><i class="fa fa-folder-open-o"></i> <span>Mon dossier</span></a></li>
        @if(Auth::user()->is_done == false)
        <li class="{{ request()->is('information/*') ? 'active' : '' }}">
            <a href="{{ route('information.index') }}">
                <i id="information-item" class="fa fa-circle-o"></i> <span>Mes Informations Personnelles</span>
            </a>
        </li>
        <li class="{{ request()->is('diploma') ? 'active' : '' }}">
            <a href="{{ route('diploma.index') }}">
                <i class="fa fa-circle-o"></i> <span>Mon parcours et mes diplômes</span>
            </a>
        </li>
        @endif
        <li><a href="#"><i class="fa fa-info-circle"></i> <span>Conditions d'accès</span></a></li>
        <li><a href="#"><i class="fa fa-exclamation"></i> <span>Objectif de la formation</span></a></li>
        <li>
            <a href="#" target="_blank">
                <i class="fa fa-book"></i> <span>Licence Professionnelle</span>
            </a>
        </li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1 id="filiere">
        Licence Professionnelle
        <small>LP</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> ESTO</a></li>
        <li class="active">@yield('breadcrumb')</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
        @yield('content')  
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Promotion 2017/2018
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2018 <a href="#">ESTO</a>.</strong> All rights reserved.
  </footer>

</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('js/adminlte.min.js') }}"></script>
@stack('scripts')
</body>
</html>