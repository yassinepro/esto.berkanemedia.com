@extends('layouts.apply')

@section('title', 'ESTO | Mon parcours et mes diplômes #2')

@section('breadcrumb', 'Mon parcours et mes diplômes')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Mon dossier</h3>
      </div>
      <!-- /.box-header -->
      <form role="form" action="{{ route('information.store') }}" method="post">
        @csrf
        <div class="box-body">
          <div class="box-group" id="accordion">
            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
            <div class="panel box box-primary">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a href="{{ route('information.create') }}">
                    Mes Informations Personnelles #1
                  </a>
                </h4>
              </div>
            </div>
            <div class="panel box box-danger">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a href="{{ route('diploma.create') }}">
                    Mon parcours et mes diplômes #2
                  </a>
                </h4>
              </div>
              <div id="collapseTwo" class="panel-collapse collapse in">
                <div class="box-body">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="cv">Mon Curriculum Vitae (CV)</label>
                      <input type="file" id="cv">

                      <p class="help-block">Vous pouvez ajouter votre CV au format PDF, PNG ou JPEG.</p>
                    </div>
                  </div>
                  <div class="col-md-7">  
                    <label>Mon cursus</label>
                    <p class="help-block">
                      Votre parcours doit comporter au moins 2 activités et toutes vos activités doivent être approuvées par au moins un justificatif.
                    </p>
                  </div>
                  <div class="col-md-4" style="padding-top:30px;">
                    <select id="cursus" class="form-control">
                        <option value="diplome">Diplôme (bac + 2)</option>
                        <option value="bac">Baccalauréat</option>
                        <option value="autre">Autre</option>
                    </select>
                  </div>
                  <div class="col-md-1" style="padding-top:30px;">
                    <button id="cursus-btn" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-diplome">Ajouter</button>
                  </div>
                  <div id="cursus" class="col-md-12">
                      <strong>Aucun élément.</strong>
                      <table style="display:none;" class="table table-striped">
                          <tbody id="cursus-content">
                          <tr>
                            <th style="width: 10px">#</th>
                            <th>Année d'obtention</th>
                            <th>Type de diplôme</th>
                            <th style="width: 40px">Action</th>
                          </tr>
                        </tbody>
                      </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="panel box box-success">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a href="{{ route('home') }}">
                    Prérequis & Validation #3
                  </a>
                </h4>
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
      </form>
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
  <div class="col-md-12">
    
  </div>
</div>


<div class="modal fade" id="modal-bac">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Baccalauréat</h4>
      </div>
      <div class="modal-body">
        <div class="form-group col-md-12">
          <label for="">Année d'obtention * :</label>
          <select id="year_of_graduation" class="form-control">
              <option value=""> </option>
              @for ($i = date('Y'); $i > 1989; $i--)
                <option value="{{ $i }}">{{ $i }}</option>  
              @endfor
          </select>
        </div>
        <div class="form-group col-md-12">
          <label>Pays * :</label>
          <select id="country" name="pays" class="form-control">
              <option value=""></option>
              @foreach($pays as $pay)
                <option value="{{ $pay->nom_fr_fr }}">{{ $pay->nom_fr_fr}}</option>
              @endforeach
          </select>
        </div>
        <div class="form-group col-md-12">
          <label for="">Province / état / région * :</label>
          <select id="region" class="form-control">
              <option value=""></option>
              @foreach ($regions as $region)
                <option value="{{ $region->id }}">{{ $region->region }}</option>
              @endforeach
          </select>
        </div>

        <div class="form-group col-md-12">
          <label for="">Ville * :</label>
          <select id="city" class="form-control">
          </select>
          <p class="help-block">Si elle n'est pas présente dans la liste, je sélectionne la mention Autre et je saisis son nom ci-dessous :</p>
          <input type="text" class="form-control">
        </div>
        <div class="form-group col-md-12">
          <label for="">CNE * :</label>
          <input id="cne" type="text" class="form-control">
        </div>
        <div class="form-group col-md-6">
          <label for="">Moyenne :</label>
          <input id="average" type="number" class="form-control">
        </div>
        <div style="clear:both;"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Annuler</button>
        <button data-dismiss="modal" id="add-bac" type="button" class="btn btn-primary">Enregistrer</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-diplome">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Diplôme d'études supérieures</h4>
      </div>
      <div class="modal-body">
        <div class="form-group col-md-12">
          <label for="">Année d'obtention * :</label>
          <select id="year_of_graduation" class="form-control">
              <option value=""> </option>
              @for ($i = date('Y'); $i > 1989; $i--)
                <option value="{{ $i }}">{{ $i }}</option>  
              @endfor
          </select>
        </div>
        <div class="form-group col-md-12">
          <label>Pays * :</label>
          <select id="diplome-country" name="pays" class="form-control">
              <option value=""></option>
              @foreach($pays as $pay)
                <option value="{{ $pay->nom_fr_fr }}">{{ $pay->nom_fr_fr}}</option>
              @endforeach
          </select>
        </div>
        <div class="form-group col-md-12">
          <label for="">Province / état / région * :</label>
          <select id="diplome-region" class="form-control">
              <option value=""></option>
              @foreach ($regions as $region)
                <option value="{{ $region->id }}">{{ $region->region }}</option>
              @endforeach
          </select>
        </div>

        <div class="form-group col-md-12">
          <label for="">Ville * :</label>
          <select id="diplome-city" class="form-control">
          </select>
          <p class="help-block">Si elle n'est pas présente dans la liste, je sélectionne la mention Autre et je saisis son nom ci-dessous :</p>
          <input type="text" class="form-control">
        </div>
        <!-- <div class="form-group col-md-12">
          <label for="">Etablissement * :</label>
          <p class="help-block">
            Si l'établissement n'est pas présent dans la liste, je sélectionne la mention Autre et je saisis son nom ci-dessous :
          </p>
          <input type="text" class="form-control">
        </div> -->
        <div class="form-group col-md-12">
          <label for="">Type de diplôme * :</label>
          <select id="dimpole-type" class="form-control">
              <option value=""></option>
              <option value="DUT">DUT Informatique</option>
              <option value="DEUP">DEUP Informatique</option>
              <option value="Autre">Autre</option>
          </select>
        </div>
        <div class="form-group col-md-12">
          <label for="">Intitulé exact du diplôme * :</label>
          <input id="entitled" type="text" class="form-control">
        </div>
        
        <fieldset class="col-md-12" style="border:1px solid #e5e5e5;">
            <legend style="width:auto; padding:0px 10px;border:none;">Moyenne générale</legend>
        
            <div class="form-group col-md-6">
              <label for="">Première année :</label>
              <input id="first-average" type="number" class="form-control">
            </div>
            <div class="form-group col-md-6">
              <label for="">Deuxième Année :</label>
              <input id="second-average" type="number" class="form-control">
            </div>
        </fieldset>
        <div style="clear:both;"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Annuler</button>
        <button data-dismiss="modal" id="add-diplome" type="button" class="btn btn-primary">Enregistrer</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

@endsection

@push('scripts')
<!-- InputMask -->
<script src="{{ asset('js/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('js/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('js/input-mask/jquery.inputmask.extensions.js') }}"></script>

<script src="{{ asset('js/intl-tel-input/js/intlTelInput.js') }}"></script>
<script>
  $("#phone").intlTelInput();
</script>
<script>
  $(function() {
    //Datemask dd/mm/yyyy
    $('#datemask, #datemask1').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
  });
</script>
<script>
    $(function(){

        $('#cursus').change(function(){
            var content = '#modal-' + $(this).val();
            $('#cursus-btn').attr('data-target',content);
        });


        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });

        $('#add-bac').click(function(){
            $.post("{{ route('diploma.store')}}",
            {
                year_of_graduation: $('#year_of_graduation').val(),
                country: $('#country').val(),
                region: $('#region').val(),
                city: $('#city').val(),
                cne: $('#cne').val(),
                average: $('#average').val(),
                first_average: null,
                second_average: null,
                type: 'Baccalauréat'
            },
            function(data){
                data = eval('(' + data + ')');
                $('#cursus > strong').attr('style','display:none;');
                $('#cursus > table').attr('style','display:table;');
                $('#cursus > table > tbody').append(`
                        <tr>
                          <td>${ data.id }.</td>
                          <td>${ data.year_of_graduation }</td>
                          <td>${ data.type }</td>
                          <td>
                            <button type="button" class="btn btn-danger btn-xs">Supprimer</button>
                          </td>
                        </tr>
                    `);                
            });
        });

        $('#region').change(function(){

            $.post("{{ route('villes')}}",
            {
                region: $('#region').val()
            },
            function(data){
                data = eval('(' + data + ')');
                $('#city').html('<option value=""></option>');
                $.each( data, function( key, item ) {
                  $('#city').append('<option value="'+item.id+'">'+ item.ville +'</option>');
                });
                $('#city').append('<option value="Autre">Autre</option>');
            });
        });

        $('#contact-region').change(function(){

            $.post("{{ route('villes')}}",
            {
                region: $('#contact-region').val()
            },
            function(data){
                data = eval('(' + data + ')');
                $('#contact-city').html('<option value=""></option>');
                $.each( data, function( key, item ) {
                  $('#contact-city').append('<option value="'+item.id+'">'+ item.ville +'</option>');
                });
                $('#contact-city').append('<option value="Autre">Autre</option>');
            });
        });

        function valider(){
            alert();
        }

        $('#certify').change(function(){
            if($('#certify').is(':checked') == true){
                $('#certify-btn').removeClass('disabled');
                $('#certify-btn').attr('type','submit');
                $('#certify-btn').attr('onclick','return true;');
            }else{
                $('#certify-btn').addClass('disabled');
                $('#certify-btn').attr('onclick','return false;');
            }
        });

        $('#diplome-region').change(function(){

            $.post("{{ route('villes')}}",
            {
                region: $('#diplome-region').val()
            },
            function(data){
                data = eval('(' + data + ')');
                $('#diplome-city').html('<option value=""></option>');
                $.each( data, function( key, item ) {
                  $('#diplome-city').append('<option value="'+item.id+'">'+ item.ville +'</option>');
                });
                $('#diplome-city').append('<option value="Autre">Autre</option>');
            });
        });
    });
</script>
@endpush