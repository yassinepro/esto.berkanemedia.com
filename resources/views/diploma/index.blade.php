@extends('layouts.apply')

@section('title', 'ESTO | Mon parcours et mes diplômes #2')

@section('breadcrumb', 'Mon parcours et mes diplômes')

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="box box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Mon dossier</h3>
      </div>
      <!-- /.box-header -->
        <div class="box-body">
          <div class="box-group" id="accordion">
            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
            <div class="panel box box-primary">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a href="{{ route('information.index') }}">
                    Mes Informations Personnelles #1
                  </a>
                </h4>
              </div>
            </div>
            <div class="panel box box-danger">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                    Mon parcours et mes diplômes #2
                  </a>
                </h4>
              </div>
              <div id="collapseTwo" class="panel-collapse collapse in">
                <div class="box-body">
                  <div class="col-md-12">
                      <div class="form-group">
                        <label for="cv">Mon Curriculum Vitae (CV) <i style="display:none;" id="upload_icon" class="fa fa-refresh fa-spin"></i></label>
                        
                        @if(count($curriculumVitaes) > 0)
                        <form method="post" action="{{ route('cv.destroy',['id' => $curriculumVitaes[0]->id]) }}">
                        @method('DELETE')
                        @csrf
                        <div id="cv_link">
                          <a class="btn btn-xs btn-primary" target="_blank" href="{{ route('welcome') }}/uploads/images/{{ $curriculumVitaes[0]->cv }}">Consulter</a>
                          <button type="submit" class="btn btn-xs btn-danger">Supprimer</button>
                        </div>
                        </form>
                        @else
                        <form id="upload-form" method="post" action="{{ route('cv.store') }}" enctype="multipart/form-data">
                        @csrf
                          <div id="upload">
                            <input type="file" name="cv" id="cv" accept="application/pdf,image/png,image/jpg">
                            <p class="help-block">Vous pouvez ajouter votre CV au format PDF, PNG ou JPEG.</p>
                          </div>
                          </form>
                        @endif
                      </div>
                  </div>
                  <div class="col-md-7">  
                    <label>Mon cursus</label>
                    <p class="help-block">
                      Votre parcours doit comporter 2 activités (Baccalauréat + Diplôme).
                    </p>
                  </div>
                  <div class="col-md-4" style="padding-top:30px;">
                    <select id="cursus" class="form-control">
                        <option value="diplome">Diplôme (bac + 2)</option>
                        <option value="bac">Baccalauréat</option>
                        <!-- <option value="autre">Autre</option> -->
                    </select>
                  </div>
                  <div class="col-md-1" style="padding-top:30px;">
                    <button id="cursus-btn" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-diplome">Ajouter</button>
                  </div>
                  <div style="margin-top:20px;" id="cursus" class="col-md-12">
                      @if(count($diplomas) > 0)
                      <table class="table table-striped">
                          <tbody id="cursus-content">
                          <tr>
                            <th style="width: 10px">#</th>
                            <th>Année d'obtention</th>
                            <th>Type de diplôme</th>
                            <th style="width: 40px">Action</th>
                          </tr>
                          @foreach($diplomas as $item)
                          <tr>
                            <td>{{ $loop->iteration }}.</td>
                            <td>{{ $item->year_of_graduation }}</td>
                            <td>{{ $item->type }}</td>
                            <td>
                              <form action="{{ route('diploma.destroy',['id' => $item->id]) }}" method="post">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-xs">Supprimer</button>
                              </form>
                            </td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                      @else
                        <strong>Aucun élément.</strong>
                      @endif
                  </div>
                </div>
              </div>
            </div>
            <div class="panel box box-success">
              <div class="box-header with-border">
                <h4 class="box-title">
                  <a href="{{ route('home') }}">
                    Prérequis & Validation #3
                  </a>
                </h4>
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
  <div class="col-md-12">
    
  </div>
</div>

<form  id="diploma-form" role="form" action="{{ route('diploma.store') }}" method="post">
@csrf
<div class="modal fade" id="modal-bac">
  <div class="modal-dialog">
    <div class="modal-content">
    <form  id="diploma-form" role="form" action="{{ route('diploma.store') }}" method="post">
      @csrf
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Baccalauréat</h4>
      </div>
      <div class="modal-body">
        <div class="form-group col-md-12 {{ $errors->has('year_of_graduation') ? 'has-error' : ''}}">
          <label for="">Année d'obtention * :</label>
          <select id="year_of_graduation" name="year_of_graduation" class="form-control">
              <option value=""> </option>
              @for ($i = date('Y'); $i > 1989; $i--)
                <option value="{{ $i }}" {{ old('year_of_graduation') == $i ? 'selected' : '' }}>{{ $i }}</option>  
              @endfor
          </select>
        </div>
        <div class="form-group col-md-12 {{ $errors->has('entitled') ? 'has-error' : ''}}">
          <label for="">La série * :</label>
          <select id="entitled" name="entitled" class="form-control">
              <option value="{{ old('entitled') }}" selected>{{ old('entitled') }}</option>
              <option value="Sciences Mathématiques">Sciences Mathématiques</option>
              <option value="Sciences Physiques">Sciences Physiques</option>
              <option value="Sciences Expérimentales">Sciences Expérimentales</option>
              <option value="Autre">Autre</option>
          </select>
          <p class="help-block">Si elle n'est pas présente dans la liste, je sélectionne la mention Autre et je saisis son nom ci-dessous :</p>
          <input type="text" name="other_entitled" value="{{ old('other_entitled') }}" class="form-control">
        </div>
        <div class="form-group col-md-12 {{ $errors->has('country') ? 'has-error' : ''}}">
          <label>Pays * :</label>
          <select id="country" name="country" class="form-control">
              <option value=""></option>
              @foreach($pays as $pay)
                <option value="{{ $pay->nom_fr_fr }}" {{ old('country') == $pay->nom_fr_fr ? 'selected' : '' }}>{{ $pay->nom_fr_fr}}</option>
              @endforeach
          </select>
        </div>
        <div class="form-group col-md-12 {{ $errors->has('region') ? 'has-error' : ''}}">
          <label for="">Province / état / région * :</label>
          <select id="region" name="region" class="form-control">
              <option value=""></option>
              @foreach ($regions as $region)
                <option value="{{ $region->id }}" {{ old('region') == $region->id ? 'selected' : '' }}>{{ $region->region }}</option>
              @endforeach
          </select>
        </div>

        <div class="form-group col-md-12 {{ $errors->has('city') ? 'has-error' : ''}}">
          <label for="">Ville * :</label>
          <select id="city" name="city" class="form-control">
          @if (old('city'))
            <option value="{{ old('city') }}">{{ old('city') }}</option>
          @endif
          </select>
          <p class="help-block">Si elle n'est pas présente dans la liste, je sélectionne la mention Autre et je saisis son nom ci-dessous :</p>
          <input type="text" name="other_city" value="{{ old('other_city') }}" class="form-control">
        </div>
        <div class="form-group col-md-12 {{ $errors->has('cne') ? 'has-error' : ''}}">
          <label for="">CNE * :</label>
          <input id="cne" value="{{ old('cne') }}" name="cne" type="text" class="form-control">
        </div>
        <div class="form-group col-md-6 {{ $errors->has('average') ? 'has-error' : ''}}">
          <label for="">Moyenne :</label>
          <input id="average" value="{{ old('average') }}" name="average" type="number" step="0.01" class="form-control">
        </div>
        <div style="clear:both;"></div>
        <input type="hidden" name="type" value="Baccalauréat">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Annuler</button>
        <button type="submit" class="btn btn-primary">Enregistrer</button>
      </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" id="modal-diplome">
  <div class="modal-dialog">
    <div class="modal-content">
    <form  id="diploma-form" role="form" action="{{ route('diploma.store') }}" method="post">
    @csrf
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Diplôme d'études supérieures</h4>
      </div>
      <div class="modal-body">
        <div class="form-group col-md-12 {{ $errors->has('year_of_registration') ? 'has-error' : ''}}">
          <label for="">Année d'inscription * :</label>
          <select id="year_of_registration" name="year_of_registration" class="form-control">
              <option value=""> </option>
              @for ($i = date('Y'); $i > 1989; $i--)
                <option value="{{ $i }}" {{ old('year_of_registration') == $i ? 'selected' : '' }}>{{ $i }}</option>  
              @endfor
          </select>
        </div>
        <div class="form-group col-md-12 {{ $errors->has('year_of_graduation') ? 'has-error' : ''}}">
          <label for="">Année d'obtention * :</label>
          <select id="year_of_graduation" name="year_of_graduation" class="form-control">
              <option value=""> </option>
              @for ($i = date('Y'); $i > 1989; $i--)
                <option value="{{ $i }}" {{ old('year_of_graduation') == $i ? 'selected' : '' }}>{{ $i }}</option>  
              @endfor
          </select>
        </div>
        <div class="form-group col-md-12 {{ $errors->has('number_year_stop') ? 'has-error' : ''}}">
          <label for="">le nombre d'année sans activité apres l'obtention du diplôme * :</label>
          <input value="{{ old('number_year_stop') }}" name="number_year_stop" type="number"  step="1" class="form-control">
        </div>
        <div class="form-group col-md-12 {{ $errors->has('country') ? 'has-error' : ''}}">
          <label>Pays * :</label>
          <select id="diplome-country" name="country" class="form-control">
              <option value=""></option>
              @foreach($pays as $pay)
                <option value="{{ $pay->nom_fr_fr }}" {{ old('country') == $pay->nom_fr_fr ? 'selected' : '' }}>{{ $pay->nom_fr_fr}}</option>
              @endforeach
          </select>
        </div>
        <div class="form-group col-md-12 {{ $errors->has('region') ? 'has-error' : ''}}">
          <label for="">Province / état / région * :</label>
          <select id="diplome-region" name="region" class="form-control">
              <option value=""></option>
              @foreach ($regions as $region)
                <option value="{{ $region->id }}" {{ old('region') == $region->id ? 'selected' : '' }}>{{ $region->region }}</option>
              @endforeach
          </select>
        </div>

        <div class="form-group col-md-12 {{ $errors->has('city') ? 'has-error' : ''}}">
          <label for="">Ville * :</label>
          <select id="diplome-city" name="city" class="form-control">
          @if (old('city'))
            <option value="{{ old('city') }}">{{ old('city') }}</option>
          @endif
          </select>
          <p class="help-block">Si elle n'est pas présente dans la liste, je sélectionne la mention Autre et je saisis son nom ci-dessous :</p>
          <input type="text" name="other_city" value="{{ old('other_city') }}" class="form-control">
        </div>
        <div class="form-group col-md-12 {{ $errors->has('type') ? 'has-error' : ''}}">
          <label for="">Type de diplôme * :</label>
          <select id="dimpole-type" name="type" class="form-control">
                <option value=""></option>
                <option value="DUT" {{ old('type') == "DUT" ? 'selected' : '' }}>DUT</option>
                <option value="DEUP" {{ old('type') == "DEUP" ? 'selected' : '' }}>DEUP</option>
                <option value="DTS" {{ old('type') == "DTS" ? 'selected' : '' }}>DTS</option>
                <option value="BTS" {{ old('type') == "BTS" ? 'selected' : '' }}>BTS</option>
                <option value="Autre" {{ old('type') == "Autre" ? 'selected' : '' }}>Autre</option>
          </select>
          <p class="help-block">Si il n'est pas présent dans la liste, je sélectionne la mention Autre et je saisis son nom ci-dessous :</p>
          <input type="text" name="other_type" value="{{ old('other_type') }}" class="form-control">
        </div>
        <div class="form-group col-md-12 {{ $errors->has('entitled') ? 'has-error' : ''}}">
          <label for="">Intitulé exact du diplôme * :</label>
          <input id="entitled" value="{{ old('entitled') }}" name="entitled" type="text" class="form-control">
        </div>
        
        <fieldset class="col-md-12" style="border:1px solid #e5e5e5;">
            <legend style="width:auto; padding:0px 10px;border:none;">Moyenne générale</legend>
        
            <div class="form-group col-md-6 {{ $errors->has('first_average') ? 'has-error' : ''}}">
              <label for="">Première année :</label>
              <input id="first-average" value="{{ old('first_average') }}" name="first_average" type="number"  step="0.01" class="form-control">
            </div>
            <div class="form-group col-md-6 {{ $errors->has('second_average') ? 'has-error' : ''}}">
              <label for="">Deuxième Année :</label>
              <input id="second-average" value="{{ old('second_average') }}" name="second_average" type="number" step="0.01" class="form-control">
            </div>
        </fieldset>
        <div style="clear:both;"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Annuler</button>
        <button type="submit" class="btn btn-primary">Enregistrer</button>
      </div>
      </form>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

</form>

@endsection

@push('scripts')
<!-- InputMask -->
<script src="{{ asset('js/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ asset('js/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('js/input-mask/jquery.inputmask.extensions.js') }}"></script>

<script src="{{ asset('js/intl-tel-input/js/intlTelInput.js') }}"></script>
<script>
  $("#phone").intlTelInput();
</script>
<script>
  $(function() {
    //Datemask dd/mm/yyyy
    $('#datemask, #datemask1').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
  });
</script>
<script>
    $(function(){

      @if ($errors->any())
        @if(old('type') == "Baccalauréat")
          $('#modal-bac').modal({show: true});
        @else
          $('#modal-diplome').modal({show: true});
        @endif
      @endif

        $('#cursus').change(function(){
            var content = '#modal-' + $(this).val();
            $('#cursus-btn').attr('data-target',content);
        });

        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });


        $("#upload-form").on('change', (function(ev) {
            //ev.preventDefault();
            $('#upload_icon').show();
            $.ajax({
                xhr: function() {
                    var xhr = $.ajaxSettings.xhr();
                    return xhr;
                },
                url: '{{ route('cv.store')}}',
                type: 'POST',
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function(data, status, xhr) {
                    // ...
                    //$('#cv_link').show();
                    //$('#upload').hide();
                    //$('#upload_icon').hide();
                    window.location.reload();

                },
                error: function(xhr, status, error) {
                    // ...
                }
           });
        }));

        $('#add-bac').click(function(){
            $.post("{{ route('diploma.store')}}",
            {
                year_of_graduation: $('#year_of_graduation').val(),
                country: $('#country').val(),
                region: $('#region').val(),
                city: $('#city').val(),
                cne: $('#cne').val(),
                average: $('#average').val(),
                first_average: null,
                second_average: null,
                type: 'Baccalauréat'
            },
            function(data){
                data = eval('(' + data + ')');
                $('#cursus > strong').attr('style','display:none;');
                $('#cursus > table').attr('style','display:table;');
                $('#cursus > table > tbody').append(`
                        <tr>
                          <td>${ data.id }.</td>
                          <td>${ data.year_of_graduation }</td>
                          <td>${ data.type }</td>
                          <td>
                            <button type="button" class="btn btn-danger btn-xs">Supprimer</button>
                          </td>
                        </tr>
                    `);                
            });
        });

        $('#region').change(function(){

            $.post("{{ route('villes')}}",
            {
                region: $('#region').val()
            },
            function(data){
                data = eval('(' + data + ')');
                $('#city').html('<option value=""></option>');
                $.each( data, function( key, item ) {
                  $('#city').append('<option value="'+item.ville+'">'+ item.ville +'</option>');
                });
                $('#city').append('<option value="Autre">Autre</option>');
            });
        });

        $('#contact-region').change(function(){

            $.post("{{ route('villes')}}",
            {
                region: $('#contact-region').val()
            },
            function(data){
                data = eval('(' + data + ')');
                $('#contact-city').html('<option value=""></option>');
                $.each( data, function( key, item ) {
                  $('#contact-city').append('<option value="'+item.id+'">'+ item.ville +'</option>');
                });
                $('#contact-city').append('<option value="Autre">Autre</option>');
            });
        });

        function valider(){
            alert();
        }

        $('#certify').change(function(){
            if($('#certify').is(':checked') == true){
                $('#certify-btn').removeClass('disabled');
                $('#certify-btn').attr('type','submit');
                $('#certify-btn').attr('onclick','return true;');
            }else{
                $('#certify-btn').addClass('disabled');
                $('#certify-btn').attr('onclick','return false;');
            }
        });

        $('#diplome-region').change(function(){

            $.post("{{ route('villes')}}",
            {
                region: $('#diplome-region').val()
            },
            function(data){
                data = eval('(' + data + ')');
                $('#diplome-city').html('<option value=""></option>');
                $.each( data, function( key, item ) {
                  $('#diplome-city').append('<option value="'+item.ville+'">'+ item.ville +'</option>');
                });
                $('#diplome-city').append('<option value="Autre">Autre</option>');
            });
        });
    });
</script>
@endpush