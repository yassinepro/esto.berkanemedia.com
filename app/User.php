<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;

    use SoftDeletes;
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function personal_informations(){
        return $this->hasOne(PersonalInformation::class);
    }

    public function contact_informations(){
        return $this->hasOne(ContactInformation::class);
    }

    public function diplomas(){
        return $this->hasMany(Diploma::class);
    }

    public function prerequisites(){
        return $this->hasOne(Prerequisite::class);
    }

    public function curriculum_vitaes(){
        return $this->hasOne(CurriculumVitae::class);
    }
}
