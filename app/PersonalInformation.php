<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PersonalInformation extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'user_id','image','family_name','first_name','gender','birth_date','native_country','place_of_birth','type_id','id_number',
    ];
	
    public function user(){
        return $this->belongsTo(User::class);
    }
}
