<?php

namespace App\Http\Controllers;

use App\PersonalInformation;
use App\ContactInformation;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PersonalInformationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(\Auth::user()->is_done)
        {
            return redirect()->route('home');
        }

        $personalInformation = PersonalInformation::where('user_id',\Auth::user()->id)->first();

        if($personalInformation)
        {
            return redirect()->route('information.show',['id' => $personalInformation->id]);
        }

        return redirect()->route('information.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $personalInformation = PersonalInformation::where('user_id',\Auth::user()->id)->first();

        if($personalInformation)
        {
            return redirect()->route('information.show',['id' => $personalInformation->id]);
        }

        $regions = DB::select('select * from region');
        $pays = DB::select('select * from pays');

        return view('information.create', array(
                'regions'=> $regions,
                'pays'=> $pays
            ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png|max:1024',
            'family_name' => 'required',
            'first_name' => 'required',
            'gender' => 'required',
            'birth_date' => 'required',
            'native_country' => 'required',
            'place_of_birth' => 'required',
            'type_id' => 'required',
            'id_number' => 'required|without_spaces|unique:personal_informations',
            'address' => 'required',
            'zip_code' => 'required',
            'region' => 'required',
            'city' => 'required',
            'phone' => 'required',
        ],[
            'id_number.without_spaces' => 'Please write without spaces',
            'id_number.required' => 'Numéro de pièce d\'identité is required',
        ]);

        //dd($request->all());
        $image = $request->image;
        $new_image_name = time().$image->getClientOriginalName();

        $image->move('uploads/images',$new_image_name);

        $informations = PersonalInformation::create([
            'user_id' => \Auth::user()->id,
            'image' => $new_image_name,
            'family_name' => $request->family_name,
            'first_name' => $request->first_name,
            'gender' => $request->gender,
            'birth_date' => $request->birth_date,
            'native_country' => $request->native_country,
            'place_of_birth' => $request->place_of_birth,
            'type_id' => $request->type_id,
            'id_number' => $request->id_number,
        ]);
        
        $city = ($request->city == 'Autre') ?  $request->auther_city : $request->city ;

        $contact = ContactInformation::create([
            'user_id' => \Auth::user()->id,
            'address' => $request->address,
            'zip_code' => $request->zip_code,
            'region' => $request->region,
            'city' => $city,
            'phone' => $request->phone,
        ]);

        $user = User::find(\Auth::user()->id);
        $user->avatar = $new_image_name;
        $user->save();

        return redirect('home');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PersonalInformation  $personalInformation
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $personalInformation = PersonalInformation::where('id',$id)->where('user_id',\Auth::user()->id)->first();

        $contactInformation = ContactInformation::where('user_id',\Auth::user()->id)->first();

        $regions = DB::select('select * from region');
        $pays = DB::select('select * from pays');
        //dd($personalInformation->family_name);
        if($personalInformation){
            return view('information.show', array(
                    'personalInformation' => $personalInformation,
                    'contactInformation' => $contactInformation,
                    'regions'=> $regions,
                    'pays'=> $pays
                ));
        }else{
            return 'lol';
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PersonalInformation  $personalInformation
     * @return \Illuminate\Http\Response
     */
    public function edit(PersonalInformation $personalInformation)
    {
        echo "string2";
        dd($personalInformation->family_name);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PersonalInformation  $personalInformation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $personalInformation = PersonalInformation::find($id);
        
        if($request->image)
        {
            $request->validate([
                'image' => 'required|image|mimes:jpeg,png|max:100',
                'family_name' => 'required',
                'first_name' => 'required',
                'gender' => 'required',
                'birth_date' => 'required',
                'native_country' => 'required',
                'place_of_birth' => 'required',
                'type_id' => 'required',
                'id_number' => "required|without_spaces|unique:personal_informations,id_number,$id",
                'address' => 'required',
                'zip_code' => 'required',
                'region' => 'required',
                'city' => 'required',
                'phone' => 'required',
            ],[
                'id_number.without_spaces' => 'Please write without spaces',
            ]);

            $image = $request->image;
            $new_image_name = time().$image->getClientOriginalName();
            $image->move('uploads/images',$new_image_name);

            $personalInformation->image = $new_image_name;

        }else
        {
            $request->validate([
                'family_name' => 'required',
                'first_name' => 'required',
                'gender' => 'required',
                'birth_date' => 'required',
                'native_country' => 'required',
                'place_of_birth' => 'required',
                'type_id' => 'required',
                'id_number' => "required|without_spaces|unique:personal_informations,id_number,$id",
                'address' => 'required',
                'zip_code' => 'required',
                'region' => 'required',
                'city' => 'required',
                'phone' => 'required',
            ],[
                'id_number.without_spaces' => 'Please write without spaces',
            ]);

        }
        
        $personalInformation->family_name = $request->family_name;
        $personalInformation->first_name = $request->first_name;
        $personalInformation->gender = $request->gender;
        $personalInformation->birth_date = $request->birth_date;
        $personalInformation->native_country = $request->native_country;
        $personalInformation->place_of_birth = $request->place_of_birth;
        $personalInformation->type_id = $request->type_id;
        $personalInformation->id_number = $request->id_number;
        $personalInformation->save();


        // address = $request->
        // zip_code = $request->
        // region = $request->
        // city = $request->
        // phone = $request->

        return redirect('home');
        dd($personalInformation->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PersonalInformation  $personalInformation
     * @return \Illuminate\Http\Response
     */
    public function destroy(PersonalInformation $personalInformation)
    {
        //
    }
}
