<?php

namespace App\Http\Controllers;

use App\Prerequisite;
use Illuminate\Http\Request;

class PrerequisiteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $prerequisite = Prerequisite::where('user_id',\Auth::user()->id)->first();
        
        $prerequisite_list = null;
        if($request->input('prerequisites_list'))
        {
            $prerequisite_list = implode(',', $request->input('prerequisites_list'));
        }

        if($prerequisite)
        {
            $prerequisite->user_id = \Auth::user()->id;
            $prerequisite->prerequisites_list = $prerequisite_list;
            $prerequisite->save();
        }else {
            $prerequisite = new Prerequisite;
            $prerequisite->user_id = \Auth::user()->id;
            $prerequisite->prerequisites_list = $prerequisite_list;
            $prerequisite->save();
        }

        //return  redirect('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Prerequisite  $prerequisite
     * @return \Illuminate\Http\Response
     */
    public function show(Prerequisite $prerequisite)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Prerequisite  $prerequisite
     * @return \Illuminate\Http\Response
     */
    public function edit(Prerequisite $prerequisite)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Prerequisite  $prerequisite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Prerequisite $prerequisite)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Prerequisite  $prerequisite
     * @return \Illuminate\Http\Response
     */
    public function destroy(Prerequisite $prerequisite)
    {
        //
    }
}
