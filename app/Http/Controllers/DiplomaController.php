<?php

namespace App\Http\Controllers;

use App\Diploma;
use App\CurriculumVitae;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DiplomaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(\Auth::user()->is_done)
        {
            return redirect()->route('home');
        }

        $regions = DB::select('select * from region');
        $pays = DB::select('select * from pays');

        $diplomas = Diploma::where('user_id',\Auth::user()->id)->get();

        $cvs = CurriculumVitae::where('user_id',\Auth::user()->id)->get();

        return view('diploma.index', array(
                'regions'=> $regions,
                'pays'=> $pays,
                'diplomas'=> $diplomas,
                'curriculumVitaes'=> $cvs
            ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $regions = DB::select('select * from region');
        $pays = DB::select('select * from pays');

        return view('diploma.create', array(
                'regions'=> $regions,
                'pays'=> $pays
            ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $diploma_count;
        if($request->type == "Baccalauréat")
        {
            $request->validate([
                'year_of_graduation' => 'required',
                'country' => 'required',
                'region' => 'required',
                'city' => 'required',
                'cne' => 'required|without_spaces|unique:diplomas,cne',
                'average' => 'required|numeric|max:20|min:0',
                'type' => 'required',
                'entitled' => 'required',
            ],[
                'cne.without_spaces' => 'Please write without spaces',

            ]);

            $diploma_count = Diploma::where('user_id',\Auth::user()->id)->where('type','Baccalauréat')->count();

        } else {

            $request->validate([
                'year_of_registration' => 'required',
                'year_of_graduation' => 'required',
                'number_year_stop' => 'required|numeric|min:0',
                'country' => 'required',
                'region' => 'required',
                'city' => 'required',
                'first_average' => 'required|numeric|max:20|min:0',
                'second_average' => 'required|numeric|max:20|min:0',
                'type' => 'required',
                'entitled' => 'required',
            ],[
                'cne.without_spaces' => 'Please write without spaces',

            ]);

            $diploma_count = Diploma::where('user_id',\Auth::user()->id)->where('type','<>','Baccalauréat')->count();
        }

        //dd($diploma_count);
        if($diploma_count < 1)
        {
            $entitled = ($request->entitled == 'Autre') ? $request->other_entitled : $request->entitled;
            $city = ($request->city == 'Autre') ? $request->other_city : $request->city;
            $type = ($request->type == 'Autre') ? $request->other_type : $request->type;

            $diploma = new Diploma;
            $diploma->user_id = \Auth::user()->id;
            $diploma->year_of_registration = $request->year_of_registration;
            $diploma->year_of_graduation = $request->year_of_graduation;
            $diploma->number_year_stop = $request->number_year_stop;
            $diploma->country = $request->country;
            $diploma->region = $request->region;
            $diploma->city = $city;
            $diploma->cne = $request->cne;
            $diploma->average = $request->average;
            $diploma->first_average = $request->first_average;
            $diploma->second_average = $request->second_average;
            $diploma->type = $type;
            $diploma->entitled = $entitled;
            $diploma->save();
        }

        return redirect()->route('diploma.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Diploma  $diploma
     * @return \Illuminate\Http\Response
     */
    public function show(Diploma $diploma)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Diploma  $diploma
     * @return \Illuminate\Http\Response
     */
    public function edit(Diploma $diploma)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Diploma  $diploma
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Diploma $diploma)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Diploma  $diploma
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $diploma = Diploma::find($id);

        $diploma->forceDelete();

        return redirect()->route('diploma.index');
        
    }
}
