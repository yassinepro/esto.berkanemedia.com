<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index()
    {
        if(\Auth::user())
        {
            return  redirect('home');
        } else{
            $params = DB::select('select * from params');
            //dd($params);
            $config;
            foreach ($params as $key => $value) {
                //dd($value->name);
                /**
                 * filiere
                 * abbreviation
                 * prerequis
                 * avis
                 * email
                 * tel
                 * fix
                 * fax
                */
                switch ($value->name) {
                    case 'filiere':
                        $config['filiere'] = $value->value;
                        break;

                    case 'abbreviation':
                        $config['abbreviation'] = $value->value;
                        break;
                    
                    case 'prerequis':
                        $config['prerequis'] = $value->value;
                        break;

                    case 'avis':
                        $config['avis'] = $value->value;
                        break;

                    case 'email':
                        $config['email'] = $value->value;
                        break;
                    
                    case 'tel':
                        $config['tel'] = $value->value;
                        break;

                    case 'fix':
                        $config['fix'] = $value->value;
                        break;

                    case 'fax':
                        $config['fax'] = $value->value;
                        break;
                }
            }

            $config = (object) $config;
            //dd($config->tel);

            
            return view('welcome',array(
                'config' => $config
            ));
        }
    }
}
