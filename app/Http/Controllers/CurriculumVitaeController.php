<?php

namespace App\Http\Controllers;

use App\CurriculumVitae;
use Illuminate\Http\Request;

class CurriculumVitaeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            "cv" => "required|mimes:pdf"
        ]);

        $cv = $request->cv;
        $new_cv_name = time().$cv->getClientOriginalName();

        $cv->move('uploads/images',$new_cv_name);

        $curriculumVitae = new CurriculumVitae;

        $curriculumVitae->cv = $new_cv_name;
        $curriculumVitae->original_name = $cv->getClientOriginalName();
        $curriculumVitae->user_id = \Auth::user()->id;
        $curriculumVitae->save();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CurriculumVitae  $curriculumVitae
     * @return \Illuminate\Http\Response
     */
    public function show(CurriculumVitae $curriculumVitae)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CurriculumVitae  $curriculumVitae
     * @return \Illuminate\Http\Response
     */
    public function edit(CurriculumVitae $curriculumVitae)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CurriculumVitae  $curriculumVitae
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CurriculumVitae $curriculumVitae)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CurriculumVitae  $curriculumVitae
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $curriculumVitae = CurriculumVitae::find($id);

        $cv_path = public_path().'/uploads/images/'.$curriculumVitae->cv;
        unlink($cv_path);

        $curriculumVitae->delete();

        return redirect()->route('diploma.index');
    }
}
