<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\PersonalInformation;
use App\ContactInformation;
use App\Prerequisite;
use App\Diploma;
use App\CurriculumVitae;
use App\User;
use App\Role;
use PDF;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $params = DB::select('select value from params where name= :prerequis',['prerequis' =>'prerequis']);
        $prerequis = explode(',', $params[0]->value);
        
        if(\Auth::user()->is_admin)
        {
            //return \Auth::user()->id;
            $users;
            $users_statistique;
            $valide = null;            
            //$role = Role::where('user_id',\Auth::user()->id)->first();
            if(\Auth::user()->role == 'scolarite'){
            
                $users = DB::table('users')
                ->join('personal_informations', 'users.id', '=', 'personal_informations.user_id')
                ->join('contact_informations', 'users.id', '=', 'contact_informations.user_id')
                ->join('diplomas', 'users.id', '=', 'diplomas.user_id')
                ->join('prerequisites', 'users.id','=', 'prerequisites.user_id')
                ->select('users.is_valid','personal_informations.*','diplomas.first_average','diplomas.second_average','diplomas.type','diplomas.year_of_registration','diplomas.year_of_graduation','diplomas.number_year_stop','contact_informations.phone','prerequisites.prerequisites_list')
                //->where('users.is_valid',$valide)
                ->where('users.is_admin',0)
                ->where('users.is_done',1)
                ->where('diplomas.type','!=','Baccalauréat')
                ->get();

                $users_statistique = DB::table('users')
                ->join('diplomas', 'users.id', '=', 'diplomas.user_id')
                ->select('diplomas.type', DB::raw('COUNT(*) as total'))
                //->where('users.is_valid',$valide)
                ->where('users.is_admin',0)
                ->where('users.is_done',1)
                ->whereNull('diplomas.deleted_at')
                ->where('diplomas.type','!=','Baccalauréat')
                ->groupBy('diplomas.type')
                ->get();

            } elseif(\Auth::user()->role == 'admin') {
                $users = DB::table('users')
                //->join('personal_informations', 'users.id', '=', 'personal_informations.user_id')
                //->join('contact_informations', 'users.id', '=', 'contact_informations.user_id')
                ->select('users.*')
                //->where('users.is_valid',1)
                ->where('users.is_admin',0)
                //->where('diplomas.type','!=','Baccalauréat')
                ->get();

                $total = DB::table('users')
                //->join('personal_informations', 'users.id', '=', 'personal_informations.user_id')
                //->join('contact_informations', 'users.id', '=', 'contact_informations.user_id')
                ->select(DB::raw('COUNT(*) as total'))
                //->where('users.is_valid',1)
                ->where('users.is_admin',0)
                //->where('diplomas.type','!=','Baccalauréat')
                ->get();

                $users_statistique = DB::table('users')
                ->join('diplomas', 'users.id', '=', 'diplomas.user_id')
                ->select('diplomas.type', DB::raw('COUNT(*) as total'))
                ->where('users.is_valid',1)
                ->where('users.is_admin',0)
                ->where('users.is_done',1)
                ->whereNull('diplomas.deleted_at')
                ->where('diplomas.type','!=','Baccalauréat')
                ->groupBy('diplomas.type')
                ->get();
            } elseif(\Auth::user()->role == 'chef') {
                $users = DB::table('users')
                ->join('personal_informations', 'users.id', '=', 'personal_informations.user_id')
                ->join('contact_informations', 'users.id', '=', 'contact_informations.user_id')
                ->join('diplomas', 'users.id', '=', 'diplomas.user_id')
                ->join('prerequisites', 'users.id','=', 'prerequisites.user_id')
                ->select('users.is_valid','personal_informations.*','diplomas.first_average','diplomas.second_average','diplomas.type','diplomas.year_of_registration','diplomas.year_of_graduation','diplomas.number_year_stop','contact_informations.phone','prerequisites.prerequisites_list')
                ->where('users.is_valid',1)
                ->where('users.is_admin',0)
                ->where('diplomas.type','!=','Baccalauréat')
                ->get();

                $users_statistique = DB::table('users')
                ->join('diplomas', 'users.id', '=', 'diplomas.user_id')
                ->select('diplomas.type', DB::raw('COUNT(*) as total'))
                ->where('users.is_valid',1)
                ->where('users.is_admin',0)
                ->where('users.is_done',1)
                ->whereNull('diplomas.deleted_at')
                ->where('diplomas.type','!=','Baccalauréat')
                ->groupBy('diplomas.type')
                ->get();
            }else{
                return 'erreur 404';
            }
            
            //dd($users_statistique);
            $statistique = array();
            $statistique['DUT'] = 0;
            $statistique['DEUP'] = 0;
            $statistique['Autre'] = 0;

            $total_members = 0;
            foreach($users_statistique as $item)
            {
                $statistique[$item->type] = $item->total;
                $total_members += $item->total;
            }

            $statistique['Autre'] = $total_members - ( $statistique['DUT'] + $statistique['DEUP'] );
            
            //dd($role->role)

            return view(\Auth::user()->role,array(
                'total_members' => $total_members,
                'total_dut' => $statistique['DUT'],
                'total_deup' => $statistique['DEUP'],
                'total_autre' => $statistique['Autre'],
                'users' => $users,
            ));
        }
        
        if(\Auth::user()->is_done)
        {
            $diploma = Diploma::where('user_id',\Auth::user()->id)->where('type', '<>', 'Baccalauréat')->first();
            $prerequisite = Prerequisite::where('user_id',\Auth::user()->id)->first();
            $personalInformation = PersonalInformation::where('user_id',\Auth::user()->id)->first();
            $cne = DB::select('select cne from diplomas where type=:type and user_id = :id', ['id'=>\Auth::user()->id,'type'=>'Baccalauréat']);
            $year =  \Auth::user()->created_at;

            $prerequisite_list = explode(',', $prerequisite->prerequisites_list);

            $params = DB::select('select * from params');
            //dd($params);
            $config;
            foreach ($params as $key => $value) {
                //dd($value->name);
                /**
                 * filiere
                 * abbreviation
                 * prerequis
                 * avis
                 * email
                 * tel
                 * fix
                 * fax
                */
                switch ($value->name) {
                    case 'filiere':
                        $config['filiere'] = $value->value;
                        break;
    
                    case 'abbreviation':
                        $config['abbreviation'] = $value->value;
                        break;
                    
                    case 'prerequis':
                        $config['prerequis'] = $value->value;
                        break;
    
                    case 'avis':
                        $config['avis'] = $value->value;
                        break;
    
                    case 'email':
                        $config['email'] = $value->value;
                        break;
                    
                    case 'tel':
                        $config['tel'] = $value->value;
                        break;
    
                    case 'fix':
                        $config['fix'] = $value->value;
                        break;
    
                    case 'fax':
                        $config['fax'] = $value->value;
                        break;

                    case 'dossier':
                        $config['dossier'] = $value->value;
                        break;
                }
            }
            $config = (object) $config;
            
            $data = array(
                'diploma'=> $diploma,
                'prerequisite'=> $prerequisite_list,
                'information'=> $personalInformation,
                'prerequis' => $prerequis,
                'config' => $config,
                'cne' => $cne[0]->cne,
                'year' => $year->format('Y'),
            );
            return view('pdf.done', $data);
        }

        
        //$prerequis = (object) $prerequis;
        //dd($prerequis);
        //$diploma = DB::select('select count(*) as total from diplomas where user_id = :id',['id' => \Auth::user()->id]);
        $information = PersonalInformation::where('user_id',\Auth::user()->id)->count();
        $diploma = Diploma::where('user_id',\Auth::user()->id)->count();
        $cv = CurriculumVitae::where('user_id',\Auth::user()->id)->count();
        $prerequisites = Prerequisite::where('user_id',\Auth::user()->id)->first();
        $personalInformation = PersonalInformation::where('user_id',\Auth::user()->id)->first();

        $prerequisite_list = [];
        if($prerequisites)
        {
            if($prerequisites->prerequisites_list)
            {
                $prerequisite_list = explode(',', $prerequisites->prerequisites_list);
            }
        }
        //dd($prerequisite_list);
        //dd($prerequisite_list);
        // $personalInformation = DB::select('select * from personal_informations where user_id = :id',['id' => \Auth::user()->id]);
        
        return view('home', array(
                'information'=> $information,
                'diploma'=> $diploma,
                'cv'=> $cv,
                'prerequisites'=> $prerequisite_list,
                'prerequisite' => $prerequisites,
                'prerequis' => $prerequis,
                'personalInformation'=> $personalInformation,
            ));
    }

    public function export() {

        if(\Auth::user()->is_admin)
        {
            // $valide = null;            
            // $role = Role::where('user_id',\Auth::user()->id)->first();
            // if($role->role == 'chef' or $role->role == 'admin'){
            //     $valide = 1;
            // }else {
            //     $valide = 0;
            // }


            $users;
            $users_statistique;
            $valide = null;            
            //$role = Role::where('user_id',\Auth::user()->id)->first();
            if(\Auth::user()->role == 'admin'){
                $users = DB::table('users')
                ->join('personal_informations', 'users.id', '=', 'personal_informations.user_id')
                ->join('contact_informations', 'users.id', '=', 'contact_informations.user_id')
                ->join('diplomas', 'users.id', '=', 'diplomas.user_id')
                ->join('prerequisites', 'users.id', '=', 'prerequisites.user_id')
                ->select('personal_informations.*','diplomas.first_average','diplomas.second_average','diplomas.type','diplomas.year_of_registration','diplomas.year_of_graduation','diplomas.number_year_stop','contact_informations.phone','prerequisites.prerequisites_list')
                ->where('users.is_admin',0)
                ->where('diplomas.type','!=','Baccalauréat')
                ->get();
            } elseif(\Auth::user()->role == 'scolarite') {
                $users_diplome = DB::table('users')
                ->join('personal_informations', 'users.id', '=', 'personal_informations.user_id')
                ->join('contact_informations', 'users.id', '=', 'contact_informations.user_id')
                ->join('diplomas', 'users.id', '=', 'diplomas.user_id')
                ->join('prerequisites', 'users.id', '=', 'prerequisites.user_id')
                ->select('personal_informations.*','diplomas.entitled','diplomas.first_average','diplomas.second_average','diplomas.type','diplomas.year_of_registration','diplomas.year_of_graduation','diplomas.number_year_stop','contact_informations.phone','prerequisites.prerequisites_list',
                    'users.*',
                    'contact_informations.*'
                    )
                ->where('users.is_valid',1)
                ->where('users.is_done',1)
                ->where('users.is_admin',0)
                ->where('diplomas.type','!=','Baccalauréat')
                ->get();
                $users_bac = DB::table('users')
                ->join('personal_informations', 'users.id', '=', 'personal_informations.user_id')
                ->join('contact_informations', 'users.id', '=', 'contact_informations.user_id')
                ->join('diplomas', 'users.id', '=', 'diplomas.user_id')
                ->join('prerequisites', 'users.id', '=', 'prerequisites.user_id')
                ->select('personal_informations.*', 'diplomas.average','diplomas.cne','diplomas.first_average','diplomas.second_average','diplomas.type','diplomas.year_of_registration','diplomas.year_of_graduation','diplomas.number_year_stop','diplomas.entitled','contact_informations.phone','prerequisites.prerequisites_list')
                ->where('users.is_valid',1)
                ->where('users.is_done',1)
                ->where('users.is_admin',0)
                ->where('diplomas.type','Baccalauréat')
                ->get();
                /*foreach ($users_diplome as $key => $value) {
                     $users_diplome[$key]['year_bac'] = $users_bac['year_of_graduation'];
                }*/
               // echo "<pre>";    
                //var_export($users_diplome);
                //var_export($users_bac);

                //die();
            } elseif(\Auth::user()->role == 'chef') {
                $users = DB::table('users')
                ->join('personal_informations', 'users.id', '=', 'personal_informations.user_id')
                ->join('contact_informations', 'users.id', '=', 'contact_informations.user_id')
                ->join('diplomas', 'users.id', '=', 'diplomas.user_id')
                ->join('prerequisites', 'users.id', '=', 'prerequisites.user_id')
                ->select('personal_informations.*','diplomas.first_average','diplomas.second_average','diplomas.type','diplomas.year_of_registration','diplomas.year_of_graduation','diplomas.number_year_stop','contact_informations.phone','prerequisites.prerequisites_list')
                ->where('users.is_admin',0)
                ->where('users.is_valid',1)
                ->where('users.is_done',1)
                ->where('diplomas.type','!=','Baccalauréat')
                ->get();
            }
            else {
                return 'erreur 404';
            }

            return view('export',array(
                'users_diplome' => $users_diplome,
                'users_bac' => $users_bac,
            ));

        } else {
            return 'erreur 404 !!';
        }
    }

    public function villes(Request $request)
    {
        $villes = DB::select('select * from ville where  region = :id', ['id'=>$request->input('region')]);

        return json_encode($villes);

    }

    public function dossier(Request $request)
    {

        $user = \Auth::user();
        $user->is_done = 1;
        $user->save();

        return  redirect('home');
    }

    public function pdf()
    {
        $params = DB::select('select value from params where name= :prerequis',['prerequis' =>'prerequis']);
        $prerequis = explode(',', $params[0]->value);

        $diploma = Diploma::where('user_id',\Auth::user()->id)->where('type', '<>', 'Baccalauréat')->first();
        $prerequisite = Prerequisite::where('user_id',\Auth::user()->id)->first();
        $personalInformation = PersonalInformation::where('user_id',\Auth::user()->id)->first();
        $cne = DB::select('select cne from diplomas where type=:type and user_id = :id', ['id'=>\Auth::user()->id,'type'=>'Baccalauréat']);
        $year =  \Auth::user()->created_at;

        $prerequisite_list = explode(',', $prerequisite->prerequisites_list);
        
        $params = DB::select('select * from params');
        //dd($params);
        $config;
        foreach ($params as $key => $value) {
            //dd($value->name);
            /**
             * filiere
             * abbreviation
             * prerequis
             * avis
             * email
             * tel
             * fix
             * fax
            */
            switch ($value->name) {
                case 'filiere':
                    $config['filiere'] = $value->value;
                    break;

                case 'abbreviation':
                    $config['abbreviation'] = $value->value;
                    break;
                
                case 'prerequis':
                    $config['prerequis'] = $value->value;
                    break;

                case 'avis':
                    $config['avis'] = $value->value;
                    break;

                case 'email':
                    $config['email'] = $value->value;
                    break;
                
                case 'tel':
                    $config['tel'] = $value->value;
                    break;

                case 'fix':
                    $config['fix'] = $value->value;
                    break;

                case 'fax':
                    $config['fax'] = $value->value;
                    break;
            }
        }
        $config = (object) $config;
        //dd($prerequisite_list);
        $data = array(
            'diploma'=> $diploma,
            'prerequisite'=> $prerequisite_list,
            'information'=> $personalInformation,
            'prerequis' => $prerequis,
            'config' => $config,
            'cne' => $cne[0]->cne,
            'year' => $year->format('Y'),
        );

        $pdf = PDF::loadView('pdf.pre', $data);
        return $pdf->download('pre.pdf');
        // return view('pdf.test', $data);
    }

    public function validation(Request $request)
    {
        $params = DB::select('select value from params where name= :prerequis',['prerequis' =>'prerequis']);
        $prerequis = explode(',', $params[0]->value);

        if(\Auth::user()->is_admin)
        {
            $user = User::find($request->code);
            $diploma = Diploma::where('user_id',$request->code)->where('type', '<>', 'Baccalauréat')->first();
            $prerequisite = Prerequisite::where('user_id',$request->code)->first();
            $personalInformation = PersonalInformation::where('user_id',$request->code)->first();
            $cne = DB::select('select cne from diplomas where type=:type and user_id = :id', ['id'=>$request->code,'type'=>'Baccalauréat']);
            $year =  $user->created_at;

            $prerequisite_list = explode(',', $prerequisite->prerequisites_list);
            $params = DB::select('select * from params');
        //dd($params);
        $config;
        foreach ($params as $key => $value) {
            //dd($value->name);
            /**
             * filiere
             * abbreviation
             * prerequis
             * avis
             * email
             * tel
             * fix
             * fax
            */
            switch ($value->name) {
                case 'filiere':
                    $config['filiere'] = $value->value;
                    break;

                case 'abbreviation':
                    $config['abbreviation'] = $value->value;
                    break;
                
                case 'prerequis':
                    $config['prerequis'] = $value->value;
                    break;

                case 'avis':
                    $config['avis'] = $value->value;
                    break;

                case 'email':
                    $config['email'] = $value->value;
                    break;
                
                case 'tel':
                    $config['tel'] = $value->value;
                    break;

                case 'fix':
                    $config['fix'] = $value->value;
                    break;

                case 'fax':
                    $config['fax'] = $value->value;
                    break;
            }
        }
        $config = (object) $config;
            $data = array(
                'diploma'=> $diploma,
                'prerequisite'=> $prerequisite_list, 
                'prerequis' => $prerequis,
                'information'=> $personalInformation,
                'information'=> $personalInformation,
                'config' => $config,
                'cne' => $cne[0]->cne,
                'year' => $year->format('Y'),
            );
            return view('pdf.validation', $data);
        }

        return 'erreur 404';

    }

    public function dossiervalide(Request $request)
    {
        $user = User::find($request->code);
        $user->is_valid = 1;
        $user->save();
    
        return  redirect('home');
    }
}
