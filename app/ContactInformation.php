<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactInformation extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];


	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'user_id','address','zip_code','region','city','phone',
    ];
	
    public function user(){
        return $this->belongsTo(User::class);
    }
}
